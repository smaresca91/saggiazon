# Saggiazon
Esame di progettazione e Sviluppo Software (Prof. Fasolino) a.a. 2016-2017

## Intro
Il progetto è stato realizzato seguendo il pattern architetturale Client Server

## Note per la Prof.
Avendo cominciato a lavorare prima che ci venisse assegnato il progetto su Gitlab,
abbiamo effettuato tutti i commit nel progetto presente a questo link:

https://gitlab.com/smaresca91/saggiazon

E' stato reso pubblico per rendere possibile la visualizzazione dello storico dei commit

## Info utili

* La connessione al database è configurata in remoto, poichè il DB è hostato su un Server in remoto

## Avviare il sistema

* modificare opportunamente il file start_serverWindows.bat con i percorsi corretti
* avviare start_serverWindows.bat
* lanciare admin.jar per provare le funzionalit� admin
* lanciare user.jar per provare le funzionalit� user
* in quest'ultimo caso si pu� utilizzare l'account di prova per il login: 
username : prova@unina.it
password : prova
