@echo off

REM Inserire qui il proprio percorso del SDK di JAVA
set JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-9.0.1.jdk/Contents/Home

REM Inserire il percorso del proprio progetto
cd /Users/salvatoremaresca/eclipse-workspace/saggiazon

REM Avvia il registry
start "%JAVA_HOME%/bin/"rmiregistry
cd bin
java -cp ..\libs\mysql-connector-java-5.1.45-bin.jar;. it.saggiazon.server.Main


REM MAC in bin
REM rmiregistry -J-Djava.rmi.servseCodebaseOnly=false
REM Start Server
REM java it.saggiazon.server.Main
