package it.saggiazon.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

public interface GestoreOffertaProxyInterface extends Remote {
	public Map<Integer, Float> mostraUtentiPremium() throws RemoteException;
	public boolean regala(int id) throws RemoteException;
}
