package it.saggiazon.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;

public interface GestoreRisorsaProxyInterface extends Remote {
	public ArrayList<String> listaAutoriCompleta() throws RemoteException;
	public int inserisciRisorsa (int id_tipo, String autore, String titolo, Date data_pubblicazione, Float prezzo_acquisto,
			Float prezzo_noleggio, String filename) throws RemoteException;
}
