package it.saggiazon.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import it.saggiazon.client.user.entity.EntityCookieClient;
import it.saggiazon.server.entity.EntityCookieServer;

public interface GestorePreferenzaProxyInterface extends Remote {
	public ArrayList<String> mostraListaAutori (int tipo) throws RemoteException;
	public int aggiungiPreferenza(EntityCookieClient cookie, String autore, int id_tipo_risorsa) throws RemoteException;
	public EntityCookieServer login(String email, String password) throws RemoteException;
}
