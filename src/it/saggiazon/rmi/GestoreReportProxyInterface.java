package it.saggiazon.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GestoreReportProxyInterface extends Remote {
	public float generaReport(int tipo, int mese, int anno) throws RemoteException;
}
