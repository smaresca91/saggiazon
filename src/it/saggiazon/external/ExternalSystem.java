package it.saggiazon.external;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class ExternalSystem {

	public static void notificaRegali(ArrayList<String> titoli, String email) {
		String subject = "Regali per il tuo catalogo personale Saggiazon";
		String email_body = "Gentile utente per premiarla della sua fedelt�, "
				+ "volevamo informarla che nel suo catalogo personale potr� trovare "+titoli.size()+" regali"
				+ "di suo interesse.";
		sendMail(email, subject, email_body);
		System.out.println("[ExternalSystem]: Email inviata a: "+email);
		for(int i = 0; i < titoli.size(); i++)
			System.out.println("Titolo regalato: "+titoli.get(i));
	}
	
	public static void notificaPreferenze(ArrayList<String> listaEmail, int id_tipo, String autore) {	
		for(String email : listaEmail) {
			String subject = "Aggiunta nuova risorsa al catalogo Saggiazon";
			String email_body = "Gentile utente volevamo informarla che nel nostro catalogo potr� trovare una nuova risorsa"
					+ "di suo interesse dell'autore "+autore+".";
			sendMail(email, subject, email_body);
			System.out.println("[ExternalSystem]: Email inviata all'indirizzo: "+email+" per la preferenza di tipo "
					+id_tipo+" e autore "+autore);
		}	
	}

	public static void sendMail(String to, String subject, String email_body) {
		
			System.out.println("Email di notifica inviata a: "+ to);		
			/*final String username = "saggiazon@gmail.com";
			final String password = "info2018@";
	
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
			
			/*final String username = "no-reply@netesi.com";
			final String password = "noreply2018@";
	
	        Properties props = new Properties();
	        props.put("mail.smtp.host", "smtps.aruba.it");
	        props.put("mail.smtp.socketFactory.port", "465");
	        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.port", "465");
	
	        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
	            @Override
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(username, password);
	            }
	        });
	        try {
	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(username));
	            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	            message.setSubject(subject);
	            message.setText(email_body);
	            Transport.send(message);
	            System.out.println("Email inviata");
	        } catch (Exception e) {
	        	System.out.println("Error email");
	            System.err.println(e);
	        }*/
	    }
	}
