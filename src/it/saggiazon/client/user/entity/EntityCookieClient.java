package it.saggiazon.client.user.entity;

import java.io.Serializable;

public class EntityCookieClient implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static EntityCookieClient ec = null;	
	private int id_utente;
	private int session_cookie;

	
	private EntityCookieClient(int id_utente,int session_cookie) {
		this.id_utente = id_utente;
		this.session_cookie = session_cookie;
	}
	
	public static EntityCookieClient getInstance(int id_utente,int session_cookie) {
		if(ec==null){
			ec = new EntityCookieClient(id_utente,session_cookie);	
			System.out.println("[Entity Cookie]: Oggetto Cookie creato!");
		}
		else {
			System.out.println("[Entity Cookie]: Oggetto Cookie gia' esistente!");
		}
		return ec;
	}

	public int getId_utente() {
		return id_utente;
	}

	public void setId_utente(int id_utente) {
		this.id_utente = id_utente;
	}

	public int getSession_cookie() {
		return session_cookie;
	}

	public void setSession_cookie(int session_cookie) {
		this.session_cookie = session_cookie;
	}

}
