package it.saggiazon.client.user.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import it.saggiazon.client.user.coordinator.GestorePreferenza;
import it.saggiazon.client.user.entity.EntityCookieClient;
import it.saggiazon.server.entity.EntityCookieServer;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class FormLogin extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private static FormLogin frame;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				Registry registry = null;
				try {
					registry = LocateRegistry.getRegistry("localhost");
					GestorePreferenza.setRegistry(registry);
					frame = new FormLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormLogin() {
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 354, 162);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);

		JLabel lblInserisciLemail = new JLabel("Inserisci l'email:");
		lblInserisciLemail.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblInserisciLemail);

		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);

		JLabel lblInserisciLaPassword = new JLabel("Inserisci la password");
		lblInserisciLaPassword.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(lblInserisciLaPassword);

		//textField_1 = new JTextField();
		//textField_1.setColumns(10);
		//panel_1.add(textField_1);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(10);
		panel_1.add(passwordField);

		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.SOUTH);

		GestorePreferenza gestorePreferenza = new GestorePreferenza();

		JButton btnConferma = new JButton("Conferma");
		btnConferma.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				EntityCookieServer cookie_s = new EntityCookieServer();
				cookie_s = gestorePreferenza.login(textField.getText(), passwordField.getText());

				if (cookie_s == null) {

					JOptionPane.showMessageDialog(contentPane, "Utente non esistente", "Login",
							JOptionPane.INFORMATION_MESSAGE);
					System.out.println("Utente non esistente!");

				} else {

					EntityCookieClient cookie_c = EntityCookieClient.getInstance(cookie_s.getId_utente(),
							cookie_s.getCookie());
					//JOptionPane.showMessageDialog(contentPane, "Utente: " + cookie_c.getId_utente(), "Login",
							//JOptionPane.INFORMATION_MESSAGE);
					System.out.println("Utente: " + cookie_c.getId_utente());
					FormAggiungiPreferito frameAP = new FormAggiungiPreferito(cookie_c);
					frameAP.setVisible(true);
					frame.setVisible(false);

				}

			}
		});
		panel_2.add(btnConferma);
	}

}
