package it.saggiazon.client.user.boundary;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;

import it.saggiazon.client.user.coordinator.GestorePreferenza;
import it.saggiazon.client.user.entity.EntityCookieClient;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class FormAggiungiPreferito extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private ArrayList<String> listAuthors;

	/**
	 * Launch the application.
	 */

	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() {
	 * 
	 * Registry registry = null; try { registry =
	 * LocateRegistry.getRegistry("localhost");
	 * GestorePreferenza.setRegistry(registry); FormAggiungiPreferito frame = new
	 * FormAggiungiPreferito(); frame.setVisible(true); } catch (Exception e) {
	 * e.printStackTrace(); } } }); }
	 */

	/**
	 * Create the frame.
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public FormAggiungiPreferito(EntityCookieClient cookie_c) {
		setResizable(false);
		setTitle("Aggiungi Preferito");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 505, 154);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		ImageIcon loading = new ImageIcon("ajax-loader.gif");
		final JLabel label = new JLabel("Loading...", loading, JLabel.CENTER);
		contentPane.add(label, BorderLayout.EAST);
		label.setVisible(false);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);

		JLabel lblTipo = new JLabel("Seleziona tipo");
		panel.add(lblTipo);

		JComboBox comboBox = new JComboBox();

		comboBox.setModel(
				new DefaultComboBoxModel(new String[] { "SELEZIONA", "File audio", "Video", "Immagine", "Podcast" }));
		comboBox.setToolTipText("");
		panel.add(comboBox);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(comboBox_1, BorderLayout.CENTER);
		// panel.add(comboBox_1);

		GestorePreferenza gestorePreferenza = new GestorePreferenza();
		listAuthors = new ArrayList<String>();
		// JLabel label = new JLabel("Loading... ", loading, JLabel.CENTER);
		// label.setVisible(false);

		/*
		 * comboBox.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent e) {
		 * 
		 * listAuthors =
		 * gestorePreferenza.mostraListaAutori(comboBox.getSelectedIndex());
		 * comboBox_1.setModel(new
		 * DefaultComboBoxModel(listAuthors.values().toArray())); /*for (Map.Entry entry
		 * : listAuthors.entrySet()) { System.out.println(entry.getKey() + ", " +
		 * entry.getValue()); }
		 */
		/*
		 * } });
		 */

		// come quella commentata ma con animazione di loading
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				label.setVisible(true);

				new SwingWorker<Void, String>() {
					@Override
					protected Void doInBackground() throws Exception {
						// Worken hard or hardly worken...
						int tipo = comboBox.getSelectedIndex();
						if (tipo == 0) {
							label.setVisible(false);
							JOptionPane.showMessageDialog(contentPane, "SELEZIONA IL TIPO!!!", "Aggiungi Preferito",
									JOptionPane.INFORMATION_MESSAGE);
							System.out.println("Tipo non selezionato!");
						} else {
							listAuthors = gestorePreferenza.mostraListaAutori(comboBox.getSelectedIndex());

							if (listAuthors.size() == 0) {
								label.setVisible(false);
								comboBox_1.setModel(new DefaultComboBoxModel());
								JOptionPane.showMessageDialog(contentPane,
										"Non ci sono autori per il tipo selezionato!!!", "Aggiungi Preferito",
										JOptionPane.INFORMATION_MESSAGE);
								System.out.println("Non ci sono autori per il tipo selezionato!");
							} else {

								comboBox_1.setModel(new DefaultComboBoxModel(listAuthors.toArray()));

							}
						}
						return null;
					}

					@Override
					protected void done() {
						label.setVisible(false);
					}
				}.execute();
			}
		});

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);

		JButton btnConferma = new JButton("Conferma");

		btnConferma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					int tipo = comboBox.getSelectedIndex();
					if (tipo == 0) {
						JOptionPane.showMessageDialog(contentPane, "SELEZIONA IL TIPO!!!", "Aggiungi Preferito",
								JOptionPane.INFORMATION_MESSAGE);
						System.out.println("Tipo non selezionato!");
					} else {
						//System.out.println(comboBox_1.getSelectedItem());
								if (gestorePreferenza.aggiungiPreferenza(cookie_c, (String) comboBox_1.getSelectedItem(), tipo) == 1) {
									  JOptionPane.showMessageDialog(contentPane,
										  "Preferenza aggiunta con successo", "Aggiungi Preferito",
										  JOptionPane.INFORMATION_MESSAGE); 
									  System.out.println("Preferenza aggiunta");
									  } 
								else if (gestorePreferenza.aggiungiPreferenza(cookie_c, (String) comboBox_1.getSelectedItem(), tipo) == -2){ 
									  JOptionPane.showMessageDialog(contentPane,
										  "Cookie non valido!!!", "Aggiungi Preferito",
									      JOptionPane.INFORMATION_MESSAGE);
									  System.out.println("Preferenza non aggiunta"); }
								else {
									JOptionPane.showMessageDialog(contentPane,
										  "Preferenza gia' presente!!!", "Aggiungi Preferito",
									      JOptionPane.INFORMATION_MESSAGE);
									System.out.println("Preferenza non aggiunta"); 			
									}
							
						}					 
					
				} catch (ArrayIndexOutOfBoundsException e) {
					JOptionPane.showMessageDialog(contentPane, "Seleziona un altro tipo!!", "Aggiungi Preferito",
							JOptionPane.INFORMATION_MESSAGE);
					System.out.println("Non posso salvare!");
				}
			}
		});
		panel_1.add(btnConferma);

		/*
		 * JComboBox comboBox_1 = new JComboBox(hs.toArray()); //comboBox_1.setModel(new
		 * DefaultComboBoxModel(hs.toArray())); //contentPane.add(comboBox_1,
		 * BorderLayout.CENTER);
		 */

	}

}
