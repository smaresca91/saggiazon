package it.saggiazon.client.user.proxy;
 

import java.rmi.registry.Registry;
import java.util.ArrayList;

import it.saggiazon.client.user.entity.EntityCookieClient;
import it.saggiazon.rmi.GestorePreferenzaProxyInterface;
import it.saggiazon.server.entity.EntityCookieServer;

public class ClientProxy {
	Registry registry;
	
	public ClientProxy(Registry registry) {
		this.registry = registry;
	}
	
	public ArrayList<String> mostraListaAutori(int tipo) {
		GestorePreferenzaProxyInterface gestorePreferenzaRemote = null;
		try {
			gestorePreferenzaRemote = (GestorePreferenzaProxyInterface) registry.lookup("gestore_preferenza");
			return gestorePreferenzaRemote.mostraListaAutori(tipo);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public EntityCookieServer login(String email, String password) {
		GestorePreferenzaProxyInterface gestorePreferenzaRemote = null;
		try {
			gestorePreferenzaRemote = (GestorePreferenzaProxyInterface) registry.lookup("gestore_preferenza");
			return gestorePreferenzaRemote.login(email, password);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public int aggiungiPreferenza(EntityCookieClient cookie, String autore, int id_tipo_risorsa) {
		GestorePreferenzaProxyInterface gestorePreferenzaRemote = null;
		try {
			gestorePreferenzaRemote = (GestorePreferenzaProxyInterface) registry.lookup("gestore_preferenza");
			return gestorePreferenzaRemote.aggiungiPreferenza(cookie,autore,id_tipo_risorsa);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	
}
