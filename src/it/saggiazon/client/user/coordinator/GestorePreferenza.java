package it.saggiazon.client.user.coordinator;

import java.rmi.registry.Registry;
import java.util.ArrayList;

import it.saggiazon.client.user.entity.EntityCookieClient;
import it.saggiazon.client.user.proxy.ClientProxy;
import it.saggiazon.server.entity.EntityCookieServer;

public class GestorePreferenza {
	private static Registry registry = null;
		
		private static Registry getRegistry(){
			return GestorePreferenza.registry;
		}
		
		public static void setRegistry(Registry r){
			GestorePreferenza.registry = r;
		}

		public EntityCookieServer login(String email, String password){
			ClientProxy clientProxy = new ClientProxy(GestorePreferenza.getRegistry());
			return clientProxy.login(email,password);
		}
		
		public int aggiungiPreferenza(EntityCookieClient cookie, String autore, int id_tipo_risorsa){
			ClientProxy clientProxy = new ClientProxy(GestorePreferenza.getRegistry());
			return clientProxy.aggiungiPreferenza(cookie,autore,id_tipo_risorsa);
		}
		
		public ArrayList<String> mostraListaAutori(int tipo){
			ClientProxy clientProxy = new ClientProxy(GestorePreferenza.getRegistry());
			return clientProxy.mostraListaAutori(tipo);
		}
		
}
