package it.saggiazon.client.admin.coordinator;

import java.rmi.registry.Registry;

import it.saggiazon.client.admin.proxy.ClientProxy;

public class GestoreReport {
	private static Registry registry = null;
	
	private static Registry getRegistry(){
		return GestoreReport.registry;
	}
	
	public static void setRegistry(Registry r){
		GestoreReport.registry = r;
	}
	
	public float generaReport(int tipo, int mese, int anno){
		ClientProxy clientProxy = new ClientProxy(GestoreReport.getRegistry());
		return clientProxy.generaReport(tipo,mese,anno);
	}

}
