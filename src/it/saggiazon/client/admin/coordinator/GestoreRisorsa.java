package it.saggiazon.client.admin.coordinator;

import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Date;

import it.saggiazon.client.admin.proxy.ClientProxy;

public class GestoreRisorsa {
	private static Registry registry = null;
		
		private static Registry getRegistry(){
			return GestoreRisorsa.registry;
		}
		
		public static void setRegistry(Registry r){
			GestoreRisorsa.registry = r;
		}
		
		public ArrayList<String> listaAutoriCompleta(){
			
			ClientProxy clientProxy = new ClientProxy(GestoreRisorsa.getRegistry());
			return clientProxy.listaAutoriCompleta();
		}
		
		public int inserisciRisorsa (int id_tipo, String autore, String titolo, Date data_pubblicazione, Float prezzo_acquisto,
				Float prezzo_noleggio, String filename) {		
				ClientProxy clientProxy = new ClientProxy(GestoreRisorsa.getRegistry());
				return clientProxy.inserisciRisorsa(id_tipo, autore, titolo, data_pubblicazione, prezzo_acquisto, prezzo_noleggio, filename);			
		}

}
