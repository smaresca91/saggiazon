package it.saggiazon.client.admin.coordinator;

import java.rmi.registry.Registry;
import java.util.Map;

import it.saggiazon.client.admin.proxy.ClientProxy;

public class GestoreOfferta {
	private static Registry registry = null;
		
		private static Registry getRegistry(){
			return GestoreOfferta.registry;
		}
		
		public static void setRegistry(Registry r){
			GestoreOfferta.registry = r;
		}
		
		public Map<Integer, Float> mostraUtentiPremium(){
			ClientProxy clientProxy = new ClientProxy(GestoreOfferta.getRegistry());
			return clientProxy.mostraUtentiPremium();
		}
		
		public boolean regala(int id) {
			ClientProxy clientProxy = new ClientProxy(GestoreOfferta.getRegistry());
			return clientProxy.regala(id);
		}

}
