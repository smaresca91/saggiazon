package it.saggiazon.client.admin.proxy;
 

import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import it.saggiazon.rmi.GestoreOffertaProxyInterface;
import it.saggiazon.rmi.GestoreReportProxyInterface;
import it.saggiazon.rmi.GestoreRisorsaProxyInterface;

public class ClientProxy {
	Registry registry;
	
	public ClientProxy(Registry registry) {
		this.registry = registry;
	}
	
	
	public ArrayList<String> listaAutoriCompleta() {
		GestoreRisorsaProxyInterface gestoreRisorsaRemote = null;
		try {
			gestoreRisorsaRemote = (GestoreRisorsaProxyInterface) registry.lookup("gestore_risorsa");
			return gestoreRisorsaRemote.listaAutoriCompleta();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public int inserisciRisorsa (int id_tipo, String autore, String titolo, Date data_pubblicazione, Float prezzo_acquisto,
			Float prezzo_noleggio, String filename) {
		GestoreRisorsaProxyInterface gestoreRisorsaRemote = null;
		try {
			gestoreRisorsaRemote = (GestoreRisorsaProxyInterface) registry.lookup("gestore_risorsa");
			return gestoreRisorsaRemote.inserisciRisorsa(id_tipo, autore, titolo, data_pubblicazione, prezzo_acquisto, prezzo_noleggio, filename);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public float generaReport (int tipo, int mese, int anno) {
		GestoreReportProxyInterface gestoreReportRemote = null;
		try {
			gestoreReportRemote = (GestoreReportProxyInterface) registry.lookup("gestore_report");
			return gestoreReportRemote.generaReport(tipo,mese,anno);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public Map<Integer, Float> mostraUtentiPremium() {
		GestoreOffertaProxyInterface gestoreOffertaRemote = null;
		try {
			gestoreOffertaRemote = (GestoreOffertaProxyInterface) registry.lookup("gestore_offerta");
			return gestoreOffertaRemote.mostraUtentiPremium();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean regala(int id) {
		GestoreOffertaProxyInterface gestoreOffertaRemote = null;
		try {
			gestoreOffertaRemote = (GestoreOffertaProxyInterface) registry.lookup("gestore_offerta");
			return gestoreOffertaRemote.regala(id);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}	
	}
	
}
