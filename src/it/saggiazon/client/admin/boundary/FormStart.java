package it.saggiazon.client.admin.boundary;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import it.saggiazon.client.admin.coordinator.GestoreOfferta;
import it.saggiazon.client.admin.coordinator.GestoreReport;
import it.saggiazon.client.admin.coordinator.GestoreRisorsa;

import javax.swing.JButton;

import javax.swing.JLabel;

import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.ParseException;


public class FormStart extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				Registry registry = null;
				try {
					registry = LocateRegistry.getRegistry("localhost");
					GestoreRisorsa.setRegistry(registry);
					GestoreReport.setRegistry(registry);
					GestoreOfferta.setRegistry(registry);
					FormStart frame = new FormStart();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormStart() {
		setResizable(false);
		setTitle("Admin Panel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 418, 418);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnAggiungiRisorsa = new JButton("AGGIUNGI RISORSA");
		btnAggiungiRisorsa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				FormAggiungiRisorsa frameAR;
				try {
					frameAR = new FormAggiungiRisorsa();
					frameAR.setVisible(true);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAggiungiRisorsa.setBounds(111, 104, 198, 56);
		contentPane.add(btnAggiungiRisorsa);

		JButton btnGeneraReport = new JButton("GENERA REPORT");
		btnGeneraReport.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				FormGeneraReport frameGR;
				frameGR = new FormGeneraReport();
				frameGR.setVisible(true);
			}
		});
		btnGeneraReport.setBounds(111, 283, 198, 56);
		contentPane.add(btnGeneraReport);

		JButton btnOffertePremium = new JButton("OFFERTE PREMIUM");
		btnOffertePremium.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				FormOffertaPremium frameOP;
				frameOP = new FormOffertaPremium();
				frameOP.setVisible(true);
			}
		});
		btnOffertePremium.setBounds(111, 189, 198, 56);
		contentPane.add(btnOffertePremium);

		JLabel lblSaggiazonProject = new JLabel("SAGGiazon Project - Esame PSSS");
		lblSaggiazonProject.setBounds(111, 37, 210, 16);
		contentPane.add(lblSaggiazonProject);

		JLabel lblEsamePsss = new JLabel("Elaborato Gruppo Maresca-Mercogliano-Piscopo-Uomo");
		lblEsamePsss.setHorizontalAlignment(SwingConstants.CENTER);
		lblEsamePsss.setBounds(36, 60, 357, 16);
		contentPane.add(lblEsamePsss);
	}
}
