package it.saggiazon.client.admin.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import java.util.stream.Collectors;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import it.saggiazon.client.admin.coordinator.GestoreOfferta;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class FormOffertaPremium extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				Registry registry = null;
				try {
					registry = LocateRegistry.getRegistry("localhost");
					GestoreOfferta.setRegistry(registry);
					FormOffertaPremium frame = new FormOffertaPremium();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public FormOffertaPremium() {

		ArrayList<Integer> listaChiavi = new ArrayList<Integer>();
		setTitle("OffertaPremium");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 368, 310);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		DefaultListModel model = new DefaultListModel();
		

		GestoreOfferta gestoreOfferta = new GestoreOfferta();
		JList list = new JList(model);
		contentPane.add(list, BorderLayout.NORTH);
		list.setVisibleRowCount(10);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnNewButton = new JButton("Regala!");
		panel_1.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(list.getSelectedValue() == null) {
					JOptionPane.showMessageDialog(contentPane, "Seleziona un utente!!!", "Offerta Premium",
							JOptionPane.INFORMATION_MESSAGE);
					System.out.println("Nessun utente selezionato!");
				} else {
					int id = listaChiavi.get(list.getSelectedIndex());
					if(gestoreOfferta.regala(id)) {
						JOptionPane.showMessageDialog(contentPane, "Regalo effettuato con successo!!!", "Offerta Premium",
								JOptionPane.INFORMATION_MESSAGE);
						System.out.println("Regalo effettuato con successo!");
					}
					else {
						JOptionPane.showMessageDialog(contentPane, "Impossibile regalare!!!", "Offerta Premium",
								JOptionPane.INFORMATION_MESSAGE);
						System.out.println("Impossibile regalare!");
					}
				}
			}
		});
		

		Map<Integer,Float> listUsers = new HashMap<Integer,Float> ();
		
		listUsers = gestoreOfferta.mostraUtentiPremium();

		LinkedHashMap<Integer, Float> sortedMap = listUsers.entrySet().stream()
		        .sorted(Map.Entry.<Integer, Float>comparingByValue().reversed()).
			    collect(Collectors.toMap(Entry::getKey, Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));
				

		int i = 0 ;
		for (Map.Entry entry : sortedMap.entrySet()) {
			model.add(i,"Utente:"+entry.getKey()+" ---> Spesa nell'anno solare: "+entry.getValue());
			listaChiavi.add((Integer) entry.getKey());
			i++;
		}
		
		
		
	}

}
