package it.saggiazon.client.admin.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import com.toedter.calendar.JYearChooser;

import it.saggiazon.client.admin.coordinator.GestoreReport;

public class FormGeneraReport extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */

	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				Registry registry = null;
				try {
					registry = LocateRegistry.getRegistry("localhost");
					GestoreReport.setRegistry(registry);
					FormGeneraReport frame = new FormGeneraReport();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public FormGeneraReport() {
		setResizable(false);
		setTitle("Genera Report");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 655, 155);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		ImageIcon loading = new ImageIcon("ajax-loader.gif");
		final JLabel label = new JLabel("Loading...", loading, JLabel.CENTER);
		contentPane.add(label, BorderLayout.EAST);
		label.setVisible(false);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
				
						JLabel lblTipo = new JLabel("Seleziona tipo");
						panel.add(lblTipo);
		
				JComboBox comboBox = new JComboBox();
				
						comboBox.setModel(
								new DefaultComboBoxModel(new String[] { "SELEZIONA", "File audio", "Video", "Immagine", "Podcast" }));
						comboBox.setToolTipText("");
						panel.add(comboBox);
						
						JLabel label_1 = new JLabel("Seleziona mese",SwingConstants.RIGHT);
						label_1.setBorder(new EmptyBorder(0,60,0,0));
						panel.add(label_1);
						
								JComboBox comboBox_1 = new JComboBox();
								panel.add(comboBox_1);
								//comboBox_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
								comboBox_1.setModel(
										new DefaultComboBoxModel(new String[] { "SELEZIONA", "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" }));
								comboBox_1.setToolTipText("");
								
								JYearChooser yearChooser_1 = new JYearChooser();
								panel.add(yearChooser_1);
		//contentPane.add(comboBox_1, BorderLayout.CENTER);
		// panel.add(comboBox_1);

		/*
		 * comboBox.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent e) {
		 * 
		 * listAuthors =
		 * gestorePreferenza.mostraListaAutori(comboBox.getSelectedIndex());
		 * comboBox_1.setModel(new
		 * DefaultComboBoxModel(listAuthors.values().toArray())); /*for (Map.Entry entry
		 * : listAuthors.entrySet()) { System.out.println(entry.getKey() + ", " +
		 * entry.getValue()); }
		 */
		/*
		 * } });
		 */

		// come quella commentata ma con animazione di loading
		/*comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				label.setVisible(true);

				new SwingWorker<Void, String>() {
					@Override
					protected Void doInBackground() throws Exception {
						// Worken hard or hardly worken...
						int tipo = comboBox.getSelectedIndex();
						if (tipo == 0) {
							label.setVisible(false);
							JOptionPane.showMessageDialog(contentPane, "SELEZIONA IL TIPO!!!", "Aggiungi Preferito",
									JOptionPane.INFORMATION_MESSAGE);
							System.out.println("Tipo non selezionato!");
						} else {
							listAuthors = gestorePreferenza.mostraListaAutori(comboBox.getSelectedIndex());
							if (listAuthors.size() == 0) {
								label.setVisible(false);
								comboBox_1.setModel(new DefaultComboBoxModel());
								JOptionPane.showMessageDialog(contentPane,
										"Non ci sono autori per il tipo selezionato!!!", "Aggiungi Preferito",
										JOptionPane.INFORMATION_MESSAGE);
								System.out.println("Non ci sono autori per il tipo selezionato!");
							} else
								comboBox_1.setModel(new DefaultComboBoxModel(listAuthors.values().toArray()));
						}
						return null;
					}

					@Override
					protected void done() {
						label.setVisible(false);
					}
				}.execute();
			}
		});*/

		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.CENTER);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setVisible(false);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_2.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
				JButton btnConferma = new JButton("Conferma");
				panel_1.add(btnConferma);
				
				btnConferma.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						label.setVisible(true);
						lblNewLabel.setVisible(false);

						new SwingWorker<Void, String>() {
							@Override
							protected Void doInBackground() throws Exception {
								// Worken hard or hardly worken...
								try {
									int tipo = comboBox.getSelectedIndex();
									if (tipo == 0) {
										JOptionPane.showMessageDialog(contentPane, "SELEZIONA IL TIPO!!!", "Genera Report",
												JOptionPane.INFORMATION_MESSAGE);
										System.out.println("Tipo non selezionato!");
									} else {
										int mese = comboBox_1.getSelectedIndex();
										//System.out.println(mese);
										if (mese == 0) {
											JOptionPane.showMessageDialog(contentPane, "SELEZIONA IL MESE!!!", "Genera Report",
													JOptionPane.INFORMATION_MESSAGE);
											System.out.println("Mese non selezionato!");
										} else {
										
											int anno = yearChooser_1.getYear();
											//	System.out.println(yearChooser_1.getYear());
											GestoreReport gestoreReport = new GestoreReport();
											float incasso = gestoreReport.generaReport(tipo, mese, anno);
											if (incasso == -1) {
												JOptionPane.showMessageDialog(contentPane, "Impossibile generare il report. Riprova tra qualche minuto!", "Genera Report",
														JOptionPane.INFORMATION_MESSAGE);
												System.out.println("Semaforo occupato");
												
											} else {
												lblNewLabel.setText("TOTALE INCASSI PER IL PERIODO SELEZIONATO: "+Float.toString(incasso)+"�");
												lblNewLabel.setVisible(true);
												}
										}
									}
									
								} catch (ArrayIndexOutOfBoundsException e) {
									JOptionPane.showMessageDialog(contentPane, "Seleziona un altro tipo!!", "Aggiungi Preferito",
											JOptionPane.INFORMATION_MESSAGE);
									System.out.println("Non posso salvare!");
								}
							
						
								return null;
							}

							@Override
							protected void done() {
								label.setVisible(false);
							}
						}.execute();
					}
				});
				
						/*btnConferma.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
				
								try {
									int tipo = comboBox.getSelectedIndex();
									if (tipo == 0) {
										JOptionPane.showMessageDialog(contentPane, "SELEZIONA IL TIPO!!!", "Genera Report",
												JOptionPane.INFORMATION_MESSAGE);
										System.out.println("Tipo non selezionato!");
									} else {
										int mese = comboBox_1.getSelectedIndex();
										//System.out.println(mese);
										if (mese == 0) {
											JOptionPane.showMessageDialog(contentPane, "SELEZIONA IL MESE!!!", "Genera Report",
													JOptionPane.INFORMATION_MESSAGE);
											System.out.println("Mese non selezionato!");
										} else {
										
										int anno = yearChooser_1.getYear();
										//	System.out.println(yearChooser_1.getYear());
										GestoreReport gestoreReport = new GestoreReport();
										float incasso = gestoreReport.generaReport(tipo, mese, anno);
										lblNewLabel.setText("TOTALE INCASSI PER IL PERIODO SELEZIONATO: "+Float.toString(incasso)+"�");
								
										}
									}
									
								} catch (ArrayIndexOutOfBoundsException e) {
									JOptionPane.showMessageDialog(contentPane, "Seleziona un altro tipo!!", "Aggiungi Preferito",
											JOptionPane.INFORMATION_MESSAGE);
									System.out.println("Non posso salvare!");
								}
							}
						});*/
						
						
						

		/*
		 * JComboBox comboBox_1 = new JComboBox(hs.toArray()); //comboBox_1.setModel(new
		 * DefaultComboBoxModel(hs.toArray())); //contentPane.add(comboBox_1,
		 * BorderLayout.CENTER);
		 */

	}

}
