package it.saggiazon.client.admin.boundary;

import java.awt.EventQueue;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import it.saggiazon.client.admin.coordinator.GestoreRisorsa;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import java.awt.Color;
import java.io.File;

public class FormAggiungiRisorsa extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField_titolo;
	private JTextField textField_prezzo_acquisto;
	private JTextField textField_prezzo_noleggio;
	private static String filename = null;

	//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private ArrayList<String> listAuthors;

	/**
	 * Launch the application.
	 */

	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				Registry registry = null;
				try {
					registry = LocateRegistry.getRegistry("localhost");
					GestoreRisorsa.setRegistry(registry);
					FormAggiungiRisorsa frame = new FormAggiungiRisorsa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 * 
	 * @throws ParseException
	 */
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void aggiornaListaAutori(JComboBox<String> comboBox_autore, JPanel panel) {
		// Autore
		GestoreRisorsa gestoreRisorsa = new GestoreRisorsa();
		listAuthors = new ArrayList<String>();
		listAuthors = gestoreRisorsa.listaAutoriCompleta();


			if (listAuthors.size() == 0) {
	
				JOptionPane.showMessageDialog(contentPane, "Non ci sono autori !", "Aggiungi Risorsa",
						JOptionPane.INFORMATION_MESSAGE);
				System.out.println("Non ci sono autori!");
	
			} else {
	
				listAuthors.add(0, "SELEZIONA");
	
				comboBox_autore.setModel(new DefaultComboBoxModel(listAuthors.toArray()));
				
	
			}

		panel.add(comboBox_autore);
		}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public FormAggiungiRisorsa() throws ParseException {

		setResizable(false);
		setTitle("Aggiungi nuova risorsa nel catalogo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 535, 284);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		//ImageIcon loading = new ImageIcon("ajax-loader.gif");
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(6, 6, 523, 250);
		contentPane.add(panel);
		panel.setLayout(null);

		// Tipo risorsa
		JLabel lblSelezionaTipo = new JLabel("Tipologia Risorsa");
		lblSelezionaTipo.setBounds(6, 9, 109, 16);
		lblSelezionaTipo.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblSelezionaTipo);

		JComboBox comboBox_tipo = new JComboBox();
		comboBox_tipo.setBounds(127, 5, 125, 27);
		comboBox_tipo.setModel(
				new DefaultComboBoxModel(new String[] { "SELEZIONA", "File audio", "Video", "Immagine", "Podcast" }));
		panel.add(comboBox_tipo);
		

		/*
		 * comboBox_tipo.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent e) { int id_tipo =
		 * comboBox_tipo.getSelectedIndex();
		 * System.out.println("ID tipologia selezionata => " + id_tipo); } });
		 */

		JSeparator separator = new JSeparator();
		separator.setBounds(287, 12, 0, 12);
		panel.add(separator);

		JLabel lblAutore = new JLabel("Autore");
		lblAutore.setBounds(264, 9, 42, 16);
		panel.add(lblAutore);

		JComboBox<String> comboBox_autore = new JComboBox<String>();
		comboBox_autore.setBounds(318, 5, 178, 27);
		
		

		/*
		 * comboBox_autore.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent e) { int id_autore =
		 * comboBox_autore.getSelectedIndex();
		 * System.out.println("ID autore selezionato => " + id_autore); } });
		 */
		

		aggiornaListaAutori(comboBox_autore,panel);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(6, 35, 511, 168);
		panel.add(panel_1);
		panel_1.setLayout(null);

		JLabel lblTitolo = new JLabel("Titolo");
		lblTitolo.setBounds(6, 20, 37, 16);
		panel_1.add(lblTitolo);

		textField_titolo = new JTextField();
		textField_titolo.setBounds(55, 15, 438, 26);
		panel_1.add(textField_titolo);
		textField_titolo.setColumns(34);

		/*JLabel lblData = new JLabel("Data Pubblicazione");
		lblData.setBounds(6, 57, 124, 16);
		panel_1.add(lblData);*/

		JLabel lblFileMultimediale = new JLabel("File Multimediale");
		lblFileMultimediale.setBounds(6, 60, 109, 16);
		panel_1.add(lblFileMultimediale);

		textField_prezzo_acquisto = new JTextField();
		textField_prezzo_acquisto.setBounds(136, 95, 109, 26);
		panel_1.add(textField_prezzo_acquisto);
		textField_prezzo_acquisto.setColumns(10);

		JLabel lblNewLabel = new JLabel("Costo di Acquisto");
		lblNewLabel.setBounds(6, 100, 124, 16);
		panel_1.add(lblNewLabel);

		JLabel lblCostoDiNoleggio = new JLabel("Costo di Noleggio");
		lblCostoDiNoleggio.setBounds(257, 100, 117, 16);
		panel_1.add(lblCostoDiNoleggio);

		textField_prezzo_noleggio = new JTextField();
		textField_prezzo_noleggio.setColumns(10);
		textField_prezzo_noleggio.setBounds(380, 95, 113, 26);
		panel_1.add(textField_prezzo_noleggio);

		JLabel lblFileUploaded = new JLabel("");
		lblFileUploaded.setBounds(6, 146, 489, 16);
		lblFileUploaded.setVisible(false);
		panel_1.add(lblFileUploaded);

		JButton btnFile = new JButton("Sfoglia");

		btnFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFileChooser chooser = new JFileChooser();

				// FileNameExtensionFilter filter = new FileNameExtensionFilter("Immagini",
				// "jpg", "jpeg", "gif", "png");
				// chooser.setFileFilter(filter);

				int returnVal = chooser.showOpenDialog(getParent());

				if (returnVal == JFileChooser.APPROVE_OPTION) {

					File file = chooser.getSelectedFile();
					filename = file.getName();
					lblFileUploaded.setText(filename);
					lblFileUploaded.setVisible(true);

				}

			}

		});

		btnFile.setBounds(136, 54, 109, 29);
		panel_1.add(btnFile);

		/*JLabel labelAiutoAnno = new JLabel("(gg/mm/aaaa)");
		labelAiutoAnno.setFont(new Font("Lucida Grande", Font.PLAIN, 8));
		labelAiutoAnno.setHorizontalAlignment(SwingConstants.CENTER);
		labelAiutoAnno.setForeground(Color.LIGHT_GRAY);
		labelAiutoAnno.setBounds(6, 72, 117, 16);
		panel_1.add(labelAiutoAnno);*/

		JLabel labelAiutoCostoAcquisto = new JLabel("in euro");
		labelAiutoCostoAcquisto.setHorizontalAlignment(SwingConstants.CENTER);
		labelAiutoCostoAcquisto.setForeground(Color.LIGHT_GRAY);
		labelAiutoCostoAcquisto.setFont(new Font("Lucida Grande", Font.PLAIN, 8));
		labelAiutoCostoAcquisto.setBounds(4, 113, 117, 16);
		panel_1.add(labelAiutoCostoAcquisto);

		JLabel labelAiutoCostoNoleggio = new JLabel("in euro");
		labelAiutoCostoNoleggio.setHorizontalAlignment(SwingConstants.CENTER);
		labelAiutoCostoNoleggio.setForeground(Color.LIGHT_GRAY);
		labelAiutoCostoNoleggio.setFont(new Font("Lucida Grande", Font.PLAIN, 8));
		labelAiutoCostoNoleggio.setBounds(255, 113, 117, 16);
		panel_1.add(labelAiutoCostoNoleggio);

		/*JFormattedTextField textField_data_pubblicazione = new JFormattedTextField(sdf);
		textField_data_pubblicazione.setBounds(137, 54, 109, 26);
		panel_1.add(textField_data_pubblicazione);
		textField_data_pubblicazione.setColumns(10);*/

		JButton btnInvia = new JButton("INVIA");
		btnInvia.setBounds(205, 215, 117, 29);
		btnInvia.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Date date = new Date();
				java.sql.Date date_sql = null;
				int check = 0;

				int id_tipo = comboBox_tipo.getSelectedIndex();
	
				String titolo = textField_titolo.getText();
				//String data_pubblicazione = textField_data_pubblicazione.getText();

				// Conversione Date
				try {
					//date = sdf.parse(data_pubblicazione);
					date_sql = new java.sql.Date(date.getTime());
					//date_sql = new java.sql.Date(date.getTime());
					Float prezzo_acquisto = Float.parseFloat(textField_prezzo_acquisto.getText());
					Float prezzo_noleggio = Float.parseFloat(textField_prezzo_noleggio.getText());

					String filename = lblFileUploaded.getText();

					GestoreRisorsa gestoreRisorsa = new GestoreRisorsa();
					
					if(id_tipo == 0){
						System.out.println("Selezionare il tipo!");
						JOptionPane.showMessageDialog(contentPane, "Selezionare il tipo!");
					} else if(comboBox_autore.getSelectedIndex() == 0){
						System.out.println("Selezionare l'autore!");
						JOptionPane.showMessageDialog(contentPane, "Selezionare l'autore!");
					} else if (titolo.equals("")) {
						System.out.println("Inserire il titolo!");
						JOptionPane.showMessageDialog(contentPane, "Inserire il titolo!");
					} else if (filename.equals("")){
						System.out.println("Aggiungere il file!");
						JOptionPane.showMessageDialog(contentPane, "Aggiungere il file!");
					} else if (prezzo_acquisto < 0) {
						System.out.println("Il prezzo di acquisto non pu� essere negativo!");
						JOptionPane.showMessageDialog(contentPane, "Il prezzo di acquisto non pu� essere negativo!");
					} else if (prezzo_noleggio < 0) {
						System.out.println("Il prezzo di noleggio non pu� essere negativo!");
						JOptionPane.showMessageDialog(contentPane, "Il prezzo di noleggio non pu� essere negativo!");
					} else {
						//System.out.println((String) comboBox_autore.getSelectedItem());
						check = gestoreRisorsa.inserisciRisorsa(id_tipo, (String) comboBox_autore.getSelectedItem(), titolo, date_sql, prezzo_acquisto,
								prezzo_noleggio, filename);

						aggiornaListaAutori(comboBox_autore,panel);
								
						}
	
						if (check == -2) {
							System.out.println("Errore: Risorsa non inserita perch� gi� esistente");
							JOptionPane.showMessageDialog(contentPane, "Errore: Risorsa non inserita perch� gi� esistente");
						} else if (check == -1) {
							System.out.println("Errore: Risorsa non inserita");
							JOptionPane.showMessageDialog(contentPane, "Errore imprevisto: Risorsa non inserita");
						} else if (check == 1) {
							System.out.println("Risorsa inserita correttamente");
							JOptionPane.showMessageDialog(contentPane, "Risorsa inserita correttamente");
						}
					} catch(NumberFormatException e){
			        //not float
					System.out.println("Formato prezzo non corretto!");
					JOptionPane.showMessageDialog(contentPane, "Formato prezzo non corretto!");
			    }
			}
				
			
		});

		panel.add(btnInvia);

		/*
		 * JComboBox comboBox_1 = new JComboBox(hs.toArray()); //comboBox_1.setModel(new
		 * DefaultComboBoxModel(hs.toArray())); //contentPane.add(comboBox_1,
		 * BorderLayout.CENTER);
		 */

	}
}