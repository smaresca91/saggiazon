package it.saggiazon.server.proxy;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import it.saggiazon.client.user.entity.EntityCookieClient;
import it.saggiazon.rmi.GestoreOffertaProxyInterface;
import it.saggiazon.rmi.GestorePreferenzaProxyInterface;
import it.saggiazon.rmi.GestoreReportProxyInterface;
import it.saggiazon.rmi.GestoreRisorsaProxyInterface;
import it.saggiazon.server.business_logic.LoginBusinessLogic;
import it.saggiazon.server.business_logic.OffertaBusinessLogic;
import it.saggiazon.server.business_logic.PreferenzaBusinessLogic;
import it.saggiazon.server.business_logic.ReportBusinessLogic;
import it.saggiazon.server.business_logic.RisorsaBusinessLogic;
import it.saggiazon.server.entity.EntityCookieServer;

public class ServerProxy implements GestorePreferenzaProxyInterface, GestoreRisorsaProxyInterface, GestoreReportProxyInterface,GestoreOffertaProxyInterface{
	
	public ArrayList<String> mostraListaAutori(int tipo) {
		PreferenzaBusinessLogic preferenzaController = new PreferenzaBusinessLogic();
		return preferenzaController.mostraListaAutori(tipo);
	}

	public EntityCookieServer login(String email, String password) {
		LoginBusinessLogic loginController = new LoginBusinessLogic();
		return loginController.login(email,password);	
	}
	
	public int aggiungiPreferenza(EntityCookieClient cookie, String autore, int id_tipo_risorsa) {
		PreferenzaBusinessLogic preferenzaController = new PreferenzaBusinessLogic();
		return preferenzaController.aggiungiPreferenza(cookie,autore,id_tipo_risorsa);
	}

	public ArrayList<String>listaAutoriCompleta()  {
		RisorsaBusinessLogic risorsaController = new RisorsaBusinessLogic();
		return risorsaController.listaAutoriCompleta();
	}
	
	public int inserisciRisorsa(int id_tipo, String autore, String titolo, Date data_pubblicazione, Float prezzo_acquisto,
			Float prezzo_noleggio, String filename) {
		RisorsaBusinessLogic risorsaController = new RisorsaBusinessLogic();
		return risorsaController.inserisciRisorsa(id_tipo, autore, titolo, data_pubblicazione, prezzo_acquisto, prezzo_noleggio, filename);
	}
	
	public float generaReport(int tipo, int mese, int anno) {
		ReportBusinessLogic reportController = new ReportBusinessLogic();
		return reportController.generaReport(tipo,mese,anno);
	}
	
	public Map<Integer, Float> mostraUtentiPremium()  {
		OffertaBusinessLogic offertaController = new OffertaBusinessLogic();
		return offertaController.mostraUtentiPremium();
	}

	public boolean regala(int id){
		OffertaBusinessLogic offertaController = new OffertaBusinessLogic();
		return offertaController.regala(id);
	}
	
}
