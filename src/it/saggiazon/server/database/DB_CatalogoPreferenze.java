package it.saggiazon.server.database;

import java.sql.ResultSet;
import java.util.ArrayList;

public class DB_CatalogoPreferenze {
	private ArrayList<DB_Preferenza> preferenze;
	
	public DB_CatalogoPreferenze() {
		super();
		preferenze = new ArrayList<DB_Preferenza>();
		caricaPreferenzeDaDb();
		System.out.println("[DB_Preferenza]: Catalogo Preferenze Caricato dal DB, preferenze presenti "+preferenze.size());
	}

	public ArrayList<DB_Preferenza> getPreferenze() {
		return preferenze;
	}

	public void setPreferenze(ArrayList<DB_Preferenza> preferenze) {
		this.preferenze = preferenze;
	}

	public void caricaPreferenzeDaDb() {
		
		try {
			ResultSet rs = DBConnectionManager.selectQuery("SELECT * FROM preferences;");
			
			while (rs.next()) {
				DB_Preferenza preferenza = new DB_Preferenza();
				
				preferenza.setId_user(rs.getInt("id_user"));
				preferenza.setId_author(rs.getInt("id_author"));
				preferenza.setId_resource_type(rs.getInt("id_resource_type"));
				
				this.preferenze.add(preferenza);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}
}
