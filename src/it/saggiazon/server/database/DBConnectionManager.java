package it.saggiazon.server.database;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class DBConnectionManager{
	public static String url = "jdbc:mysql://37.60.245.222:3306/";
	public static String dbName = "mondoaff_saggiazon";
	public static String driver = "com.mysql.jdbc.Driver";
	public static String userName = "mondoaff_saggiaz"; 
	public static String password = "saggiazon2018";

	public static Connection getConnection() throws Exception{
	  Connection conn = null;
	  //System.out.println("sono nel driver manager");
	  
	  Class.forName(driver).newInstance();
	  //System.out.println("sono dopo class forname");
	  conn = (Connection) DriverManager.getConnection(url+dbName,userName,password);
	  //System.out.println("sono dopo getconnection");
	  System.out.println(conn);
	  //System.out.println("esco dal driver manager");
	  return conn;
	}

	public static void closeConnection(Connection c) throws Exception{
		c.close();
	}
	
	public static ResultSet selectQuery(String query) throws Exception{
		Connection conn = getConnection();
        Statement statement = conn.createStatement();
        ResultSet ret = statement.executeQuery(query);
        //conn.close();
        return ret;
	}

	public static int updateQuery(String query) throws Exception{
		Connection conn = getConnection();
		Statement statement = conn.createStatement();
		int ret = statement.executeUpdate(query);
		//conn.close();
		return ret;
	}
}

