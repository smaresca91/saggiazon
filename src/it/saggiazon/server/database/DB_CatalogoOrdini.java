package it.saggiazon.server.database;

import java.sql.ResultSet;
import java.util.ArrayList;

public class DB_CatalogoOrdini {
	private ArrayList<DB_Ordine> ordini;
	
	public DB_CatalogoOrdini() {
		super();
		ordini = new ArrayList<DB_Ordine>();
		caricaOrdiniDaDb();
		System.out.println("[DB_CatalogoOrdini]: Catalogo Ordini Caricato dal DB, ordini presenti "+ordini.size());
	}
	
	public ArrayList<DB_Ordine> getOrdini() {
		return ordini;
	}

	public void setOrdini(ArrayList<DB_Ordine> ordini) {
		this.ordini = ordini;
	}

	public void caricaOrdiniDaDb() {
		
		try {
			ResultSet rs = DBConnectionManager.selectQuery("SELECT * FROM orders;");
			
			while (rs.next()) {
				DB_Ordine ordine = new DB_Ordine();
				
				ordine.setId(rs.getInt("id"));
				ordine.setId_risorsa(rs.getInt("id_resource"));
				ordine.setId_utente(rs.getInt("id_user"));
				ordine.setId_tipo(rs.getInt("type_order"));
				ordine.setData_ordine(rs.getDate("created_at"));
				ordine.setCosto(rs.getFloat("cost"));
				
				System.out.println("Data ordine: "+ordine.getData_ordine());
				
				this.ordini.add(ordine);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}
}
