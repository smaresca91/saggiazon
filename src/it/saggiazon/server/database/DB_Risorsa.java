package it.saggiazon.server.database;

import java.util.Date;

public class DB_Risorsa {
	private Integer id;
	private String title;
	private Integer type_id;
	private Date date_of_publish;
	private Float purchase_price;
	private Float rent_price;
	private String file;
	private Integer author;

	public DB_Risorsa(){
		super();
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Integer getType_id() {
		return type_id;
	}
	
	public void setType_id(Integer type_id) {
		this.type_id = type_id;
	}
	
	public Date getDate_of_publish() {
		return date_of_publish;
	}
	
	public void setDate_of_publish(Date date_of_publish) {
		this.date_of_publish = date_of_publish;
	}
	
	public Float getPurchase_price() {
		return purchase_price;
	}
	
	public void setPurchase_price(Float purchase_price) {
		this.purchase_price = purchase_price;
	}
	
	public Float getRent_price() {
		return rent_price;
	}
	
	public void setRent_price(Float rent_price) {
		this.rent_price = rent_price;
	}
	
	public String getFile() {
		return file;
	}
	
	public void setFile(String file) {
		this.file = file;
	}
	
	public Integer getAuthor() {
		return author;
	}
	
	public void setAuthor(Integer author) {
		this.author = author;
	}
	
	public int salvaInDB(){
		String query = "";
		
		query = "INSERT INTO resources(id, title, date_of_pub, purchase_price, rent_price, id_type, file, id_author) VALUES ("+this.id+",'"+this.title+"','"+this.date_of_publish+"',"+this.purchase_price+","+this.rent_price+","+this.type_id+",'"+this.file+"',"+this.author+");";
				
		try {
			DBConnectionManager.updateQuery(query);
			System.out.println("[DB_Risorsa]: Risorsa salvata nel DB!!");
			return 1;
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		}
		
	}

	public int elimina() {
		String query = "";
	
		query = "DELETE FROM resources WHERE id="+this.id+"";
				
		try {
			DBConnectionManager.updateQuery(query);
			System.out.println("[DB_Risorsa]: Risorsa eliminata dal DB!!");
			return 1;
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		}
		
	}
	

}
