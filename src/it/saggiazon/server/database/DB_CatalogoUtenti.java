package it.saggiazon.server.database;

import java.sql.ResultSet;
import java.util.ArrayList;

public class DB_CatalogoUtenti {
	private ArrayList<DB_Utente> utenti;
	
	public DB_CatalogoUtenti() {
		super();
		utenti = new ArrayList<DB_Utente>();
		caricaUtentiDaDb();
		System.out.println("[DB_CatalogoUtenti]: Catalogo Utenti Caricato dal DB, utenti presenti "+utenti.size());
	}
	
	public ArrayList<DB_Utente> getUtenti() {
		return utenti;
	}

	public void setUtenti(ArrayList<DB_Utente> utenti) {
		this.utenti = utenti;
	}

	public void caricaUtentiDaDb() {
		
		try {
			ResultSet rs = DBConnectionManager.selectQuery("SELECT * FROM users;");
			
			while (rs.next()) {
				DB_Utente utente = new DB_Utente();
				
				utente.setId(rs.getInt("id"));
				utente.setNome(rs.getString("name"));
				utente.setCognome(rs.getString("surname"));
				utente.setEmail(rs.getString("email"));
				utente.setPassword(rs.getString("password"));
				
				this.utenti.add(utente);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}
}
