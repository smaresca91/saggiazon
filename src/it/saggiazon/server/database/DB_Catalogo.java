package it.saggiazon.server.database;

import java.sql.ResultSet;
import java.util.ArrayList;

public class DB_Catalogo {
	private ArrayList<DB_Risorsa> risorse;
	
	public DB_Catalogo() {
		super();
		risorse = new ArrayList<DB_Risorsa>();
		caricaRisorseDaDb();
		System.out.println("[DB_Catalogo]: Catalogo Caricato dal DB, risorse presenti "+risorse.size());
	}

	public ArrayList<DB_Risorsa> getRisorse() {
		return risorse;
	}

	public void setRisorse(ArrayList<DB_Risorsa> risorse) {
		this.risorse = risorse;
	}

	public void caricaRisorseDaDb() {
		
		try {
			ResultSet rs = DBConnectionManager.selectQuery("SELECT * FROM resources");
		
			while (rs.next()){
				
				DB_Risorsa risorsa = new DB_Risorsa();
				
				risorsa.setId(rs.getInt("id"));
				risorsa.setTitle(rs.getString("title"));
				risorsa.setDate_of_publish(rs.getDate("date_of_pub"));
				risorsa.setPurchase_price(rs.getFloat("purchase_price"));
				risorsa.setRent_price(rs.getFloat("rent_price"));
				risorsa.setType_id(rs.getInt("id_type"));
				risorsa.setAuthor(rs.getInt("id_author"));
				
				this.risorse.add(risorsa);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}
}
