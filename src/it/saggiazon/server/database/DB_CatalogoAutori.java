package it.saggiazon.server.database;

import java.sql.ResultSet;
import java.util.ArrayList;

public class DB_CatalogoAutori {
	private ArrayList<DB_Autore> autori;
	
	public DB_CatalogoAutori() {
		super();
		autori = new ArrayList<DB_Autore>();
		caricaAutoriDaDb();
		System.out.println("[DB_CatalogoAutori]: Catalogo Autori Caricato dal DB, autori presenti "+autori.size());
	}
	
	public ArrayList<DB_Autore> getAutori() {
		return autori;
	}

	public void setAutori(ArrayList<DB_Autore> autori) {
		this.autori = autori;
	}

	public void caricaAutoriDaDb() {
	
		try {
			ResultSet rs = DBConnectionManager.selectQuery("SELECT * FROM authors;");
			
			while (rs.next()) {
				DB_Autore autore = new DB_Autore();
				Integer[] array_tipi = new Integer[4];
				
				autore.setId(rs.getInt("id"));
				autore.setNome(rs.getString("name"));
				autore.setCognome(rs.getString("surname"));
				autore.setData_di_nascita(rs.getDate("date_of_birth"));
				array_tipi[0] = rs.getInt("audio");
				array_tipi[1] = rs.getInt("video");
				array_tipi[2] = rs.getInt("immagine");
				array_tipi[3] = rs.getInt("podcast");
				autore.setTipi(array_tipi);
				
				this.autori.add(autore);
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}

}
