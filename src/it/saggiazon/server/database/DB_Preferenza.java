package it.saggiazon.server.database;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;


public class DB_Preferenza {
	private Integer id_user;
	private Integer id_author;
	private Integer id_resource_type;

	public DB_Preferenza() {
		super();
	}	

	public Integer getId_user() {
		return id_user;
	}

	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}

	public Integer getId_author() {
		return id_author;
	}

	public void setId_author(Integer id_author) {
		this.id_author = id_author;
	}

	public Integer getId_resource_type() {
		return id_resource_type;
	}

	public void setId_resource_type(Integer id_resource_type) {
		this.id_resource_type = id_resource_type;
	}

	public int salvaInDB() {
		String query = "";
	
		query = "INSERT INTO preferences VALUES("+this.id_user+",'"+this.id_author+"',"+this.id_resource_type+");";
		
		try {
			return DBConnectionManager.updateQuery(query);
		} catch (MySQLIntegrityConstraintViolationException ex) {
			System.out.println("[DB_Preferenza]: Preferenza gia' presente nel DB");
			return -1;
		} catch (Exception ex) {
			ex.printStackTrace();
			return -2;
		}
	}
}
