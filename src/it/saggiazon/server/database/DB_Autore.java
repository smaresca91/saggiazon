package it.saggiazon.server.database;

import java.util.Date;

public class DB_Autore {
	private Integer id;
	private String nome;
	private String cognome;
	private Date data_di_nascita;
	private Integer[] tipi; 
	
	public DB_Autore(){
		super();
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Date getData_di_nascita() {
		return data_di_nascita;
	}

	public void setData_di_nascita(Date data_di_nascita) {
		this.data_di_nascita = data_di_nascita;
	}
	
	public Integer[] getTipi() {
		return tipi;
	}

	public void setTipi(Integer[] tipi) {
		this.tipi = tipi;
	}

	public int salvaInDB() {
		String query = "";

		query = "INSERT INTO authors VALUES("+this.id+","+this.nome+",'"+this.cognome+"',"+this.data_di_nascita+","+this.tipi[0]+","+this.tipi[1]+","+this.tipi[2]+","+this.tipi[3]+");";

		try {
			return DBConnectionManager.updateQuery(query);
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		}
	}
	
	public int aggiornaDB() {
		String query = "";
	
		query = "UPDATE authors SET audio="+this.tipi[0]+",video="+this.tipi[1]+",immagine="+this.tipi[2]+",podcast="+this.tipi[3]+" WHERE id="+this.id+";";
		
		try {
			return DBConnectionManager.updateQuery(query);
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1; //aggiornamento fallito
		}
	}

}
