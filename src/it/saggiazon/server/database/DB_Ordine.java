package it.saggiazon.server.database;

import java.util.Date;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class DB_Ordine {
	private Integer id;
	private Integer id_risorsa;
	private Integer id_utente;
	private Integer id_tipo;
	private Float 	costo;
	private Date data_ordine;
	
	public DB_Ordine(){
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId_risorsa() {
		return id_risorsa;
	}

	public void setId_risorsa(Integer id_risorsa) {
		this.id_risorsa = id_risorsa;
	}

	public Integer getId_utente() {
		return id_utente;
	}

	public void setId_utente(Integer id_utente) {
		this.id_utente = id_utente;
	}

	public Integer getId_tipo() {
		return id_tipo;
	}

	public void setId_tipo(Integer id_tipo) {
		this.id_tipo = id_tipo;
	}

	public Float getCosto() {
		return costo;
	}

	public void setCosto(Float costo) {
		this.costo = costo;
	}

	public Date getData_ordine() {
		return data_ordine;
	}

	public void setData_ordine(Date data_ordine) {
		this.data_ordine = data_ordine;
	}
	
	public int salvaInDB() {
		String query = "";
	
		query = "INSERT INTO orders VALUES("+this.id+","+this.id_risorsa+","+this.id_utente+","+this.id_tipo+","+this.costo+",'"+this.data_ordine+"');";
		
		try {
			return DBConnectionManager.updateQuery(query);
		} catch (MySQLIntegrityConstraintViolationException ex) {
			System.out.println("[DB_Ordine]: Ordine gia' presente nel DB");
			return -1; 
		} catch (Exception ex) {
			ex.printStackTrace();
			return -2;
		}
	}
}
