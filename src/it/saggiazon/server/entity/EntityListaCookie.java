package it.saggiazon.server.entity;

import java.util.ArrayList;
import java.util.Date;


public class EntityListaCookie {
	ArrayList<EntityCookieServer> cookies;
	private static EntityListaCookie elc = null;
	
	private EntityListaCookie() {
		cookies = new ArrayList<EntityCookieServer>();
	};
	
	public static EntityListaCookie getInstance() {		
		if(elc==null){
			elc = new EntityListaCookie();
			
			System.out.println("[EntityListaCookie]:Oggetto ListaCookie Utenti creato!");
		}
		else {
			System.out.println("[EntityListaCookie]:Oggetto ListaCookie Utenti gia' esistente!");
		}
		return elc;
	}

	public ArrayList<EntityCookieServer> getCookies() {
		return cookies;
	}

	public void setCookies(ArrayList<EntityCookieServer> cookies) {
		this.cookies = cookies;
	}
	
	public void addCookie(EntityCookieServer cookie) {
		this.cookies.add(cookie);
		System.out.println("[EntityListaCookie]: Nuovo cookie server aggiunto!");
	}
	
	public boolean verifyCookie(int id_utente, int cookie) {
		for(EntityCookieServer cs: this.cookies) {
			if(cs.getId_utente().equals(id_utente) && cs.getCookie().equals(cookie)){
				if(new Date().getTime()-cs.getTime() > 30000)
					return false;
				System.out.println("[EntityListaCookie]: cookie verificato!");
				return true;
			}
		}
		return false;
	}
}
