package it.saggiazon.server.entity;

import java.util.Date;

import it.saggiazon.server.database.DB_Autore;

public class EntityAutore {		
	Integer id;
	String nome;
	String cognome;
	Date data_di_nascita;
	Integer[] tipi;
		
	public EntityAutore(){
			super();
	}
	
	public EntityAutore(DB_Autore dbAutore){
		this.id = dbAutore.getId();
		this.nome = dbAutore.getNome();
		this.cognome = dbAutore.getCognome();
		this.data_di_nascita = dbAutore.getData_di_nascita();
		this.tipi = dbAutore.getTipi();
		System.out.println("[EntityAutore]: Oggetto Autore creato "+this.nome+" "+this.cognome);
	}
	
	public String getNomeCompleto() {
		return nome +" "+ cognome;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Date getData_di_nascita() {
		return data_di_nascita;
	}

	public void setData_di_nascita(Date data_di_nascita) {
		this.data_di_nascita = data_di_nascita;
	}

	public Integer[] getTipi() {
		return tipi;
	}

	public void setTipi(Integer[] tipi) {
		this.tipi = tipi;
	}

	public void incrementaTipo(int tipo) {
		this.tipi[tipo] += 1;
	}
	
	public void decrementaTipo(int tipo) {
		this.tipi[tipo] -= 1;
	}

	public boolean aggiornaAutore(int tipo) {
		incrementaTipo(tipo-1);
		DB_Autore dbAutore = new DB_Autore();
		dbAutore.setId(this.id);
		dbAutore.setTipi(this.tipi);
	
		if(dbAutore.aggiornaDB() == 1) {
			System.out.println("[EntityAutore]: Autore "+this.getNomeCompleto()+" aggiornato nel DB");
			return true;
		}else{
			System.out.println("[EntityAutore]: Autore non aggiornato!");
			decrementaTipo(tipo-1);
			return false;
		}
	}

	@Override
    public boolean equals(Object object) {
        boolean sameSame = false;

        if (object != null && this != object && object instanceof EntityAutore) {
            sameSame = this.id.equals(((EntityAutore) object).id);
        }

        return sameSame;
    }
	
	
}
