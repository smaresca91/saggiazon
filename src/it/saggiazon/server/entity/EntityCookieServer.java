package it.saggiazon.server.entity;

import java.io.Serializable;

public class EntityCookieServer implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer id_utente;
	private Integer cookie;
	private long time;

	public EntityCookieServer() {
		super();
	}

	public Integer getId_utente() {
		return id_utente;
	}

	public void setId_utente(Integer id_utente) {
		this.id_utente = id_utente;
	}

	public Integer getCookie() {
		return cookie;
	}

	public void setCookie(Integer cookie) {
		this.cookie = cookie;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	
	

}
