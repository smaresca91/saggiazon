package it.saggiazon.server.entity;

import it.saggiazon.server.database.DB_Preferenza;

public class EntityPreferenza {
	EntityAutore author;
	EntityUtente utente;
	Integer id_resource_type;

	public EntityPreferenza(){
			super();
	}

	public EntityPreferenza(EntityAutore autore, EntityUtente utente, Integer id_resource_type){
			this.id_resource_type = id_resource_type;
			this.author = autore;
			this.utente = utente;
	}
	
	public EntityPreferenza(DB_Preferenza dbPreferenza){
		this.id_resource_type = dbPreferenza.getId_resource_type();
		this.author = EntityCatalogoAutori.getInstance().getAutore(dbPreferenza.getId_author());
		this.utente = EntityCatalogoUtenti.getInstance().getUtente(dbPreferenza.getId_user());
}
	
	public Integer getId_resource_type() {
		return id_resource_type;
	}

	public void setId_resource_type(Integer id_resource_type) {
		this.id_resource_type = id_resource_type;
	}

	public EntityAutore getAuthor() {
		return author;
	}

	public void setAuthor(EntityAutore author) {
		this.author = author;
	}

	public EntityUtente getUtente() {
		return utente;
	}

	public void setUtente(EntityUtente utente) {
		this.utente = utente;
	}

	public boolean aggiungiPreferenza(){
		DB_Preferenza dbPreferenza = new DB_Preferenza();
		
		dbPreferenza.setId_user(this.utente.getId());
		dbPreferenza.setId_author(this.author.getId());
		dbPreferenza.setId_resource_type(this.id_resource_type);
		if(dbPreferenza.salvaInDB()==1) {
			System.out.println("[EntityPreferenza]: Preferenza aggiunta al DB");
			return true;
		}else{
			System.out.println("[EntityPreferenza]: Preferenza gia' presente nel DB");
			return false;
		}
	}
	
}
