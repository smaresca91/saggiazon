package it.saggiazon.server.entity;

import java.util.ArrayList;
import java.util.Date;

import it.saggiazon.server.database.DB_CatalogoOrdini;
import it.saggiazon.server.database.DB_Ordine;

public class EntityCatalogoOrdini {
	ArrayList<EntityOrdine> ordini;

	private static Integer nextId = 0;
	private static EntityCatalogoOrdini eco = null;
	
	private EntityCatalogoOrdini() {
		ordini = new ArrayList<EntityOrdine>();	
		
		ArrayList<DB_Ordine> listDbOrdini= new ArrayList<DB_Ordine>();
		DB_CatalogoOrdini dbCatalogoOrdini = new DB_CatalogoOrdini();
		listDbOrdini = dbCatalogoOrdini.getOrdini();
		
		for(DB_Ordine ordine : listDbOrdini) {
			ordini.add(new EntityOrdine(ordine));
			if (ordine.getId() > nextId) {
				nextId = ordine.getId();
			}
		}	
		nextId = nextId + 1;
	};
	
	public static EntityCatalogoOrdini getInstance() {
		if(eco==null){
			eco = new EntityCatalogoOrdini();
			
			System.out.println("[EntityCatalogoOrdini]: Oggetto Catalogo Ordini creato!");
		}
		else {
			System.out.println("[EntityCatalogoOrdini]: Oggetto Catalogo Ordini gia' esistente!");
		}
		return eco;
	}

	public ArrayList<EntityOrdine> getOrdini() {
		return ordini;
	}

	public void setOrdini(ArrayList<EntityOrdine> ordini) {
		this.ordini = ordini;
	}
	
	private void aggiornaCatalogoOrdini(EntityOrdine ordine) {
		this.ordini.add(ordine);
		nextId = ordine.getId() + 1;
		System.out.println("[EntityCatalogoOrdine]: Aggiunto all'oggetto catalogoOrdine l'ordine "+ordine.getRisorsa().getTitle());
	}
	
	public int aggiungiOrdineAcquisto(EntityUtente utente, EntityRisorsa risorsa, float costo){
			Date date = new Date();
			java.sql.Date date_sql = new java.sql.Date(date.getTime());
			EntityOrdine ordine = new EntityOrdine(nextId,utente,risorsa,0,costo,date_sql);
			if(ordine.aggiungiOrdine()) {
				aggiornaCatalogoOrdini(ordine);
				return 1;
			} else
				return -1;				
		}
}
