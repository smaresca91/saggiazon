package it.saggiazon.server.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import it.saggiazon.server.database.DB_Utente;


public class EntityUtente implements Serializable {		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Semaphore semaforo = new Semaphore(1);
	
	Integer id;
	String nome;
	String cognome;
	String email;
	String password;
	ArrayList<EntityOrdine> ordini;

	public EntityUtente(){
			super();
	}	
	
	public EntityUtente(DB_Utente dbUtente){
		this.id = dbUtente.getId();
		this.nome = dbUtente.getNome();
		this.cognome = dbUtente.getCognome();
		this.email = dbUtente.getEmail();
		this.password = dbUtente.getPassword();
		this.ordini = new ArrayList<EntityOrdine>();
		
		System.out.println("[EntityUtente]: Oggetto Utente con ID: "+this.id+" creato. ");
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNomeCompleto() {
		return nome +" "+ cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setOrdini(ArrayList<EntityOrdine> ordini) {
		this.ordini = ordini;
	}
	
	public ArrayList<EntityOrdine> getOrdini() {
		ArrayList<EntityOrdine> listOrdini = EntityCatalogoOrdini.getInstance().getOrdini();
		
		for(EntityOrdine ordine : listOrdini) {
			if(ordine.getUtente().getId().equals(this.id)) {
				if(!this.ordini.contains(ordine))
					this.ordini.add(ordine);
			}
		}
		return this.ordini;
	}

	public ArrayList<EntityRisorsa> catalogoAcquistiPersonale() {
		System.out.println("Accesso al catalogoPersonale effettuato");

		ArrayList<EntityRisorsa> catalogoAcquistiPersonale = new ArrayList<EntityRisorsa>();		
		
		for(EntityOrdine ordine : this.getOrdini()) {
			if(ordine.getId_tipo()==0)
				catalogoAcquistiPersonale.add(ordine.getRisorsa());
		}

		return catalogoAcquistiPersonale;
	}
	
	public Semaphore getSemaforo() {
		return semaforo;
	}

	public boolean aggiungiAcquisto(EntityRisorsa risorsa, float costo) {
		EntityCatalogoOrdini catalogoOrdini = EntityCatalogoOrdini.getInstance();
		if (catalogoOrdini.aggiungiOrdineAcquisto(this, risorsa, costo) == 1)
			return true;
		else
			return false;
	}
	
}
