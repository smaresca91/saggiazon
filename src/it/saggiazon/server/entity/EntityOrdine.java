package it.saggiazon.server.entity;

import java.util.Date;

import it.saggiazon.server.database.DB_Ordine;


public class EntityOrdine {
	Integer id;
	Integer id_tipo;
	Float 	costo;
	Date data_ordine;
	EntityRisorsa risorsa;
	EntityUtente utente;
	
	public EntityOrdine(){
		super();
	}
	
	public EntityOrdine(DB_Ordine dbOrdine){
		super();
		this.id = dbOrdine.getId();
		this.id_tipo = dbOrdine.getId_tipo();
		this.costo = dbOrdine.getCosto();
		this.data_ordine = dbOrdine.getData_ordine();
		this.risorsa = EntityCatalogo.getInstance().getRisorsa(dbOrdine.getId_risorsa());
		this.utente = EntityCatalogoUtenti.getInstance().getUtente(dbOrdine.getId_utente());
	}

	public EntityOrdine(int id, EntityUtente utente, EntityRisorsa risorsa,int id_tipo, float costo, Date data){
		this.setId(id);
		this.setUtente(utente);
		this.setRisorsa(risorsa);
		this.setId_tipo(id_tipo);
		this.setCosto(costo);
		this.setData_ordine(data);
		System.out.println("[EntityOrdine]: Ordine creato!");
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EntityRisorsa getRisorsa() {
		return risorsa;
	}

	public void setRisorsa(EntityRisorsa risorsa) {
		this.risorsa = risorsa;
	}

	public Integer getId_tipo() {
		return id_tipo;
	}

	public void setId_tipo(Integer id_tipo) {
		this.id_tipo = id_tipo;
	}

	public Float getCosto() {
		return costo;
	}

	public void setCosto(Float costo) {
		this.costo = costo;
	}

	public Date getData_ordine() {
		return data_ordine;
	}

	public void setData_ordine(Date data_ordine) {
		this.data_ordine = data_ordine;
	}
	
	public EntityUtente getUtente() {
		return utente;
	}

	public void setUtente(EntityUtente utente) {
		this.utente = utente;
	}

	public boolean aggiungiOrdine() {
		DB_Ordine dbOrdine = new DB_Ordine();
		
		dbOrdine.setId(this.id);
		dbOrdine.setId_utente(this.utente.getId());
		dbOrdine.setId_risorsa(this.risorsa.getId());
		dbOrdine.setId_tipo(this.id_tipo);
		dbOrdine.setCosto(this.costo);
		dbOrdine.setData_ordine(this.data_ordine);
		if(dbOrdine.salvaInDB()==1) {
			System.out.println("[EntityOrdine]: Ordine aggiunto al DB");
			return true;
		}else{
			System.out.println("[EntityOrdine]: Ordine gia' presente nel DB");
			return false;
		}
	}
	
	@Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;

        if (object != null && object instanceof EntityOrdine)
        {
            sameSame = this.id.equals(((EntityOrdine) object).id);
        }

        return sameSame;
    }
	
}
