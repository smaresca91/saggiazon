package it.saggiazon.server.entity;

import java.util.ArrayList;

import it.saggiazon.server.database.DB_CatalogoPreferenze;
import it.saggiazon.server.database.DB_Preferenza;

public class EntityCatalogoPreferenze {
	
	ArrayList<EntityPreferenza> preferenze;
	private static EntityCatalogoPreferenze ecp = null;
	
	private EntityCatalogoPreferenze() {
		preferenze = new ArrayList<EntityPreferenza>();	
		
		ArrayList<DB_Preferenza> listDbPreferenze= new ArrayList<DB_Preferenza>();
		DB_CatalogoPreferenze dbCatalogoPreferenze = new DB_CatalogoPreferenze();
		listDbPreferenze = dbCatalogoPreferenze.getPreferenze();
		
		for(DB_Preferenza p : listDbPreferenze)
			preferenze.add(new EntityPreferenza(p));
	};
	
	public static EntityCatalogoPreferenze getInstance() {
		if(ecp==null){
			ecp = new EntityCatalogoPreferenze();	
			System.out.println("[EntityCatalogoPreferenze]: Oggetto Catalogo Preferenze creato!");
		}
		else {
			System.out.println("[EntityCatalogoPreferenze]: Oggetto Catalogo Preferenze gi� esistente!");
		}
		return ecp;
	}

	public ArrayList<EntityPreferenza> getPreferenze() {
		return preferenze;
	}

	public void setPreferenze(ArrayList<EntityPreferenza> preferenze) {
		this.preferenze = preferenze;
	}
	
	public int aggiungiPreferenza(EntityAutore autore,EntityUtente utente,int id_tipo_risorsa) {
		EntityPreferenza preferenza = new EntityPreferenza(autore,utente,id_tipo_risorsa);
		if(preferenza.aggiungiPreferenza()) {
			preferenze.add(preferenza);
			System.out.println("[EntityCatalogoPreferenze]: Aggiunto all'oggetto catalogo preferenze la preferenza con"
					+ " tipo "+id_tipo_risorsa+" e autore "+autore.getNomeCompleto());
			return 1;
		}			
		else
			return -1;
	}
	
	public ArrayList<EntityUtente> utentiDaNotificare(int id_tipo, EntityAutore autore){
		
		ArrayList<EntityUtente> utenti = new ArrayList<EntityUtente>();
		
		for(EntityPreferenza preferenza : preferenze) {
			if(preferenza.getAuthor().getId().equals(autore.getId()) && preferenza.getId_resource_type().equals(id_tipo)) {
				utenti.add(preferenza.getUtente());
			}
		}
		return utenti;
	}
	
}
