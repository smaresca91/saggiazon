package it.saggiazon.server.entity;

import java.util.ArrayList;
import java.util.Date;

import it.saggiazon.server.database.DB_Catalogo;
import it.saggiazon.server.database.DB_Risorsa;

public class EntityCatalogo {
	ArrayList<EntityRisorsa> risorse;
	
	private static Integer nextId = 0;
	private static EntityCatalogo ec = null;
	
	
	private EntityCatalogo() {
		
		risorse = new ArrayList<EntityRisorsa>();	
		ArrayList<DB_Risorsa> listDbRisorse= new ArrayList<DB_Risorsa>();
		DB_Catalogo dbCatalogo = new DB_Catalogo();
		listDbRisorse = dbCatalogo.getRisorse();
		
		for(DB_Risorsa risorsa : listDbRisorse) {
			risorse.add(new EntityRisorsa(risorsa));
			
			if (risorsa.getId() > nextId) {
				nextId = risorsa.getId();
			}
		}
		
		nextId = nextId + 1;
		
	};
	
	public static EntityCatalogo getInstance() {
		if(ec==null){
			ec = new EntityCatalogo();
			
			System.out.println("[EntityCatalogo]: Oggetto Catalogo creato!");
			System.out.println("[EntityCatalogo]: NextID:"+ec.getNextId());
		}
		else {
			System.out.println("[EntityCatalogo]: Oggetto Catalogo gia' esistente!");
			System.out.println("[EntityCatalogo]: NextID:"+ec.getNextId());
		}
		return ec;
	}

	public ArrayList<EntityRisorsa> getRisorse() {
		return risorse;
	}

	public void setRisorse(ArrayList<EntityRisorsa> risorse) {
		this.risorse = risorse;
	}
	
	public int getNextId() {
		return nextId;
	}
	
	public void setNextId(int id) {
		EntityCatalogo.nextId = id;
	}
	
	//aggiorna il catalogo aggiungendo una risorsa
	private void aggiornaCatalogo(EntityRisorsa risorsa) {
		
		this.risorse.add(risorsa);
		
		nextId = risorsa.getId() + 1;
		
		System.out.println("[EntityCatalogo]: Aggiunto all'oggetto catalogo la risorsa con ID "+risorsa.getId());
	}
	
	//restituisce una risorsa specifica del catalogo
	public EntityRisorsa getRisorsa(int id_risorsa) {
		for(EntityRisorsa r: this.risorse) {
			if(r.getId().equals(id_risorsa)){
				return r;
			}
		}
		return null;	
	}
	
	public boolean checkDuplicato(String titolo, String autore) {
		for(EntityRisorsa risorsa : risorse) {		
			if(risorsa.getTitle().equals(titolo) && risorsa.getAutore().getNomeCompleto().equals(autore)) {
				System.out.println("[EntityCatalogo]: Errore-> Titolo: "+ risorsa.getTitle()+" con AutoreId: "+risorsa.getAutore().getId()+" gia' esistente!!!");
				return true;
			}
		}
		return false;			
	}
		
	public int aggiungiRisorsa(Integer id, int id_tipo, EntityAutore autore, String titolo, Date data_pubblicazione, Float prezzo_acquisto,
		Float prezzo_noleggio, String filename) {
		EntityRisorsa entityRisorsa = new EntityRisorsa(id, id_tipo, autore, titolo, data_pubblicazione, prezzo_acquisto, prezzo_noleggio, filename);
		if(entityRisorsa.aggiungiRisorsa()) {
			if(autore.aggiornaAutore(id_tipo)) {		
				aggiornaCatalogo(entityRisorsa);
				return 1;
			}
			else {
				entityRisorsa.eliminaRisorsa();
				return -1;
			}
		}			
		else
			return -1;	
		
	}
		
}
