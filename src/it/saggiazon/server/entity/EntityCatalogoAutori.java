package it.saggiazon.server.entity;

import java.util.ArrayList;

import it.saggiazon.server.database.DB_Autore;
import it.saggiazon.server.database.DB_CatalogoAutori;

public class EntityCatalogoAutori {
	ArrayList<EntityAutore> autori;

	private static EntityCatalogoAutori eca = null;
	
	private EntityCatalogoAutori() {
		autori = new ArrayList<EntityAutore>();	
		
		ArrayList<DB_Autore> listDbAutori= new ArrayList<DB_Autore>();
		DB_CatalogoAutori dbCatalogoAutori = new DB_CatalogoAutori();
		listDbAutori = dbCatalogoAutori.getAutori();
		
		for(DB_Autore autore : listDbAutori) 
			autori.add(new EntityAutore(autore));
	}
	
	public static EntityCatalogoAutori getInstance() {
		if(eca==null){
			eca = new EntityCatalogoAutori();
			
			System.out.println("[EntityCatalogoAutori]: Oggetto Catalogo Autori creato!");
		}
		else {
			System.out.println("[EntityCatalogoAutori]: Oggetto Catalogo Autori gia' esistente!");
		}
		return eca;
	}

	public ArrayList<EntityAutore> getAutori() {
		return autori;
	}

	public void setOrdini(ArrayList<EntityAutore> autori) {
		this.autori = autori;
	}

	public EntityAutore getAutore(int id_autore) {
		for(EntityAutore a: this.autori) {
			if(a.getId().equals(id_autore)){
				return a;
			}
		}
		return null;
	}
	
	public EntityAutore getAutore(String nome_autore) {
		for(EntityAutore a: this.autori) {
			if(a.getNomeCompleto().equals(nome_autore)){
				return a;
			}
		}
		return null;
	}
	
	public ArrayList<String> getListaAutori(int tipo){
		ArrayList<String> authors = new ArrayList<String>();
		for(EntityAutore autore : autori) {
			if(autore.getTipi()[tipo-1] > 0) {
					authors.add(autore.getNomeCompleto());
					System.out.println("[EntityCatalogo]: Carico autore:"+autore.getNomeCompleto());
			}
		}
		return authors;		
	}

	public ArrayList<String> getListaAutori(){
		ArrayList<String> authors = new ArrayList<String>();
		for(EntityAutore autore : autori) {
			authors.add(autore.getNomeCompleto());
			System.out.println("[EntityCatalogo]: Carico autore:"+autore.getNomeCompleto());
		}
		return authors;		
	}
}
