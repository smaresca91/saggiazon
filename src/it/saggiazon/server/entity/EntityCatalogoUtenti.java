package it.saggiazon.server.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import it.saggiazon.server.database.DB_CatalogoUtenti;
import it.saggiazon.server.database.DB_Utente;

public class EntityCatalogoUtenti {
	
	ArrayList<EntityUtente> utenti;
	private static EntityCatalogoUtenti ecu = null;
	
	private EntityCatalogoUtenti() {
		utenti = new ArrayList<EntityUtente>();	
		
		ArrayList<DB_Utente> listDbUtenti= new ArrayList<DB_Utente>();
		DB_CatalogoUtenti dbCatalogoUtenti = new DB_CatalogoUtenti();
		listDbUtenti = dbCatalogoUtenti.getUtenti();
		
		for(DB_Utente utente : listDbUtenti)
			utenti.add(new EntityUtente(utente));
	};
	
	public static EntityCatalogoUtenti getInstance() {
		if(ecu==null){
			ecu = new EntityCatalogoUtenti();	
			System.out.println("[EntityCatalogoUtenti]: Oggetto Catalogo Utenti creato!");
		}
		else {
			System.out.println("[EntityCatalogoUtenti]: Oggetto Catalogo Utenti gi� esistente!");
		}
		return ecu;
	}

	public ArrayList<EntityUtente> getUtenti() {
		return utenti;
	}

	public void setUtenti(ArrayList<EntityUtente> utenti) {
		this.utenti = utenti;
	}
	
	public EntityUtente getUtente(int id_utente) {
		for(EntityUtente u: this.utenti) {
			if(u.getId().equals(id_utente)){
				return u;
			}
		}
		return null;
	}

	@SuppressWarnings("deprecation")	
	public Map<Integer, Float> getUtentiPremium(){
					
			Map<Integer, Float> utentiPremium = new HashMap<Integer, Float>();
			Date date = new Date();
			int anno_corrente = date.getYear()+1900;
			
			for(EntityUtente utente : utenti) {
				ArrayList<EntityOrdine> ordini = new ArrayList<EntityOrdine>();
				ordini = utente.getOrdini();
				float costo_totale = 0;
				for(EntityOrdine ordine : ordini) {
					if((ordine.getData_ordine().getYear()+1900) == anno_corrente && ordine.getId_tipo().equals(0)) {
						costo_totale += ordine.getCosto();
					}
				}
				if(costo_totale >= 100) {
					utentiPremium.put(utente.getId(), costo_totale);
				}		
			}
			return utentiPremium;
		}
		
}
