package it.saggiazon.server.entity;

import java.util.Date;

import it.saggiazon.server.database.DB_Risorsa;

public class EntityRisorsa {
	Integer id;
	String title;
	Integer type_id;
	Date date_of_publish;
	Float purchase_price;
	Float rent_price;
	String file;
	EntityAutore autore;

	public EntityRisorsa(Integer id, int id_tipo, EntityAutore autore, String titolo, Date data_pubblicazione, Float prezzo_acquisto,
			Float prezzo_noleggio, String filename){
		
		super();
		
		this.id = id;
		this.type_id = id_tipo;
		this.autore = autore;
		this.title = titolo;
		this.date_of_publish = data_pubblicazione;
		this.purchase_price = prezzo_acquisto;
		this.rent_price = prezzo_noleggio;
		this.file = filename;
		
		System.out.println("[EntityRisorsa]: Oggetto Risorsa aggiunto "+this.title);
		
	} 
	
	public EntityRisorsa(DB_Risorsa dbRisorsa){
			
			this.id = dbRisorsa.getId();
			this.title = dbRisorsa.getTitle();
			this.type_id = dbRisorsa.getType_id();
			this.date_of_publish = dbRisorsa.getDate_of_publish();
			this.purchase_price = dbRisorsa.getPurchase_price();
			this.rent_price = dbRisorsa.getRent_price();
			this.file = dbRisorsa.getFile();
			this.autore = EntityCatalogoAutori.getInstance().getAutore(dbRisorsa.getAuthor());
			
			System.out.println("[EntityRisorsa]: Oggetto Risorsa creato "+this.title+", "
					+ "autore "+this.autore.getNomeCompleto());
		
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Integer getType_id() {
		return type_id;
	}
	
	public void setType_id(Integer type_id) {
		this.type_id = type_id;
	}
	
	public Date getDate_of_publish() {
		return date_of_publish;
	}
	
	public void setDate_of_publish(Date date_of_publish) {
		this.date_of_publish = date_of_publish;
	}
	
	public Float getPurchase_price() {
		return purchase_price;
	}
	
	public void setPurchase_price(Float purchase_price) {
		this.purchase_price = purchase_price;
	}
	
	public Float getRent_price() {
		return rent_price;
	}
	
	public void setRent_price(Float rent_price) {
		this.rent_price = rent_price;
	}
	
	public String getFile() {
		return file;
	}
	
	public void setFile(String file) {
		this.file = file;
	}
	
	public EntityAutore getAutore() {
		return autore;
	}
	
	public void setAutore(EntityAutore autore) {
		this.autore = autore;
	}

	public boolean aggiungiRisorsa() {
		
		DB_Risorsa dbRisorsa = new DB_Risorsa();
		dbRisorsa.setId(this.id);
		dbRisorsa.setType_id(this.type_id);
		dbRisorsa.setAuthor(this.autore.getId());
		dbRisorsa.setTitle(this.title);
		dbRisorsa.setDate_of_publish(this.date_of_publish);
		dbRisorsa.setPurchase_price(this.purchase_price);
		dbRisorsa.setRent_price(this.rent_price);
		dbRisorsa.setFile(this.file);
	
		if(dbRisorsa.salvaInDB() == 1) {
			System.out.println("[EntityRisorsa]: Risorsa aggiunta al DB");
			return true;
		}else{
			System.out.println("[EntityRisorsa]: Risorsa gia' presente nel DB");
			return false;
		}
		
	}
	
	@Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;

        if (object != null && object instanceof EntityRisorsa)
        {
            sameSame = this.id.equals(((EntityRisorsa) object).id);
        }

        return sameSame;
    }

	public void eliminaRisorsa() {
		DB_Risorsa dbRisorsa = new DB_Risorsa();
		dbRisorsa.setId(this.id);
		dbRisorsa.elimina();
	}
	
}
