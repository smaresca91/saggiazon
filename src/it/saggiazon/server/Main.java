package it.saggiazon.server;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import it.saggiazon.rmi.GestoreOffertaProxyInterface;
import it.saggiazon.rmi.GestorePreferenzaProxyInterface;
import it.saggiazon.rmi.GestoreReportProxyInterface;
import it.saggiazon.rmi.GestoreRisorsaProxyInterface;
import it.saggiazon.server.entity.EntityCatalogo;
import it.saggiazon.server.entity.EntityCatalogoAutori;
import it.saggiazon.server.entity.EntityCatalogoPreferenze;
import it.saggiazon.server.entity.EntityCatalogoUtenti;
import it.saggiazon.server.proxy.ServerProxy;

	public class Main {

	public static void bootServer() {
		EntityCatalogoAutori.getInstance();
		EntityCatalogo.getInstance();
		EntityCatalogoUtenti.getInstance();
		EntityCatalogoPreferenze.getInstance();
	}
	
	public static void main(String[] args) throws MalformedURLException {
		
		try {
			
			Registry registry = LocateRegistry.getRegistry("localhost");
			
			ServerProxy preferenzaControllerProxy = new ServerProxy();
			ServerProxy risorsaControllerProxy = new ServerProxy();
			ServerProxy reportControllerProxy = new ServerProxy();
			ServerProxy offertaControllerProxy = new ServerProxy();
			
			GestorePreferenzaProxyInterface proxy_preferenza = (GestorePreferenzaProxyInterface) UnicastRemoteObject.exportObject(preferenzaControllerProxy, 0);
			GestoreRisorsaProxyInterface proxy_risorsa = (GestoreRisorsaProxyInterface) UnicastRemoteObject.exportObject(risorsaControllerProxy, 0);
			GestoreReportProxyInterface proxy_report = (GestoreReportProxyInterface) UnicastRemoteObject.exportObject(reportControllerProxy, 0);
			GestoreOffertaProxyInterface proxy_offerta = (GestoreOffertaProxyInterface) UnicastRemoteObject.exportObject(offertaControllerProxy, 0);
			
			registry.rebind("gestore_preferenza", proxy_preferenza);
			registry.rebind("gestore_risorsa", proxy_risorsa);
			registry.rebind("gestore_report", proxy_report);
			registry.rebind("gestore_offerta", proxy_offerta);
			
			bootServer();

            System.err.println("Server ready");
	
		} catch (RemoteException e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();		
		}
	}
	
}

