package it.saggiazon.server.business_logic;

import java.util.ArrayList;

import it.saggiazon.client.user.entity.EntityCookieClient;
import it.saggiazon.server.entity.EntityCatalogoAutori;
import it.saggiazon.server.entity.EntityCatalogoPreferenze;
import it.saggiazon.server.entity.EntityCatalogoUtenti;
import it.saggiazon.server.entity.EntityListaCookie;
import it.saggiazon.server.entity.EntityUtente;


public class PreferenzaBusinessLogic {

	public ArrayList<String> mostraListaAutori(int tipo){
		EntityCatalogoAutori catalogoAutori = EntityCatalogoAutori.getInstance();	
		return catalogoAutori.getListaAutori(tipo);		
	}
	
	public int aggiungiPreferenza(EntityCookieClient cookie, String autore, int id_tipo_risorsa) {
		
		EntityListaCookie listaCookie = EntityListaCookie.getInstance();
		if(!listaCookie.verifyCookie(cookie.getId_utente(), cookie.getSession_cookie())) {
			return -2; //problema cookie!
		}

		EntityUtente utente = EntityCatalogoUtenti.getInstance().getUtente(cookie.getId_utente());
		EntityCatalogoAutori catalogoAutori = EntityCatalogoAutori.getInstance();
		
		return(EntityCatalogoPreferenze.getInstance().aggiungiPreferenza(catalogoAutori.getAutore(autore), utente, id_tipo_risorsa));
		
	}

}
