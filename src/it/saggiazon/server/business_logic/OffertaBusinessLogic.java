package it.saggiazon.server.business_logic;

import java.util.ArrayList;
import java.util.Map;

import it.saggiazon.external.ExternalSystem;
import it.saggiazon.server.entity.EntityCatalogo;
import it.saggiazon.server.entity.EntityCatalogoUtenti;
import it.saggiazon.server.entity.EntityRisorsa;
import it.saggiazon.server.entity.EntityUtente;



public class OffertaBusinessLogic {
	
	public Map<Integer, Float> mostraUtentiPremium(){	
		EntityCatalogoUtenti catalogoUtenti = EntityCatalogoUtenti.getInstance();
		return catalogoUtenti.getUtentiPremium();
	}
	
	public boolean regala(int id_utente) {
		EntityUtente utente = EntityCatalogoUtenti.getInstance().getUtente(id_utente);
		try {
			utente.getSemaforo().acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		EntityCatalogo catalogo = EntityCatalogo.getInstance();
		ArrayList<EntityRisorsa> catalogoAcquistiPersonale = utente.catalogoAcquistiPersonale();
		ArrayList<EntityRisorsa> catalogoNonAcquistati = new ArrayList<EntityRisorsa>();
		ArrayList<String> titoliRegalati = new ArrayList<String>();
		
		for(EntityRisorsa risorsa : catalogo.getRisorse()) {
			if(!catalogoAcquistiPersonale.contains(risorsa))
				catalogoNonAcquistati.add(risorsa);
		}
		
		if(catalogoNonAcquistati.size() >= 3) {
			for(int i = 0; i < 3 ; i++ ) {
				utente.aggiungiAcquisto(catalogoNonAcquistati.get(i), 0);
				titoliRegalati.add(catalogoNonAcquistati.get(i).getTitle());
			}
			System.out.println("Regalo consegnato!");
			ExternalSystem.notificaRegali(titoliRegalati,utente.getEmail());
			utente.getSemaforo().release();
			return true;
			
		} else {
			System.out.println("Impossibile regalare!");
			utente.getSemaforo().release();
			return false;
		}
				
	}
	
	/*public ArrayList<EntityRisorsa> mostraRisorseDisponibili(EntityCookieClient cookie) {
		
		EntityListaCookie listaCookie = EntityListaCookie.getInstance();
		if(!listaCookie.verifyCookie(cookie.getId_utente(), cookie.getSession_cookie())) {
			return null; //problema cookie!
		}
		EntityCatalogo catalogo = EntityCatalogo.getInstance();
		EntityUtente utente = EntityCatalogoUtenti.getInstance().getUtente(cookie.getId_utente());
		ArrayList<EntityRisorsa> catalogoAcquistiPersonale = utente.CatalogoAcquistiPersonale(0);
		ArrayList<EntityRisorsa> catalogoNonAcquistati = new ArrayList<EntityRisorsa>();
	
		for(EntityRisorsa risorsa : catalogo.getRisorse()) {
			if(!catalogoAcquistiPersonale.contains(risorsa))
				catalogoNonAcquistati.add(risorsa);
		}
		
		return catalogoNonAcquistati;
	}
		
	public boolean acquista(EntityRisorsa risorsa, EntityCookieClient cookie) {
		EntityListaCookie listaCookie = EntityListaCookie.getInstance();
		if(!listaCookie.verifyCookie(cookie.getId_utente(), cookie.getSession_cookie())) {
			return false; //problema cookie!
		}
		EntityUtente utente = EntityCatalogoUtenti.getInstance().getUtente(cookie.getId_utente());
		
		if(utente.aggiungiAcquisto(risorsa,risorsa.getPurchase_price())) {
			utente.getSemaforoAcquisto().release();
			return true;
		}
		
		utente.getSemaforoAcquisto().release();
		return false;
	}*/
}


	