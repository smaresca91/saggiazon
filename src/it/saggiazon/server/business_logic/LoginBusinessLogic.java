package it.saggiazon.server.business_logic;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import it.saggiazon.server.entity.EntityCatalogoUtenti;
import it.saggiazon.server.entity.EntityCookieServer;
import it.saggiazon.server.entity.EntityListaCookie;
import it.saggiazon.server.entity.EntityUtente;


public class LoginBusinessLogic {	

	public EntityCookieServer login(String email, String password) {
		EntityCatalogoUtenti catalogoUtenti = EntityCatalogoUtenti.getInstance();
		for(EntityUtente utente : catalogoUtenti.getUtenti()) {
			if(utente.getEmail().equals(email) && utente.getPassword().equals(password)) {
				EntityCookieServer cookie = new EntityCookieServer();
				cookie.setId_utente(utente.getId());
				cookie.setCookie(ThreadLocalRandom.current().nextInt(0, 1001));
				cookie.setTime(new Date().getTime());
				
				System.out.println(cookie.getId_utente());
				System.out.println(cookie.getCookie());
				
				EntityListaCookie listaCookie = EntityListaCookie.getInstance();
				listaCookie.addCookie(cookie);
				return cookie;
			}
		}
		return null;	
	}
	

}
