package it.saggiazon.server.business_logic;

import java.util.concurrent.Semaphore;

import it.saggiazon.server.entity.EntityCatalogoOrdini;
import it.saggiazon.server.entity.EntityOrdine;
import it.saggiazon.server.entity.EntityRisorsa;

public class ReportBusinessLogic {
	private static Semaphore semaforo = new Semaphore(3);
	@SuppressWarnings("deprecation")
	public float generaReport(int tipo, int mese, int anno)  {
		if(semaforo.tryAcquire()) {

			System.out.println("Semaforo acquisito.");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			EntityCatalogoOrdini catalogoOrdini = EntityCatalogoOrdini.getInstance();
			float totale = 0;
			for(EntityOrdine ordine : catalogoOrdini.getOrdini()) {
				EntityRisorsa risorsa = ordine.getRisorsa();
				System.out.println("esamino la risorsa "+risorsa.getTitle());
				if(risorsa.getType_id().equals(tipo) && (ordine.getData_ordine().getMonth()+1)==mese && (ordine.getData_ordine().getYear()+1900)==anno) {
					totale += ordine.getCosto();
				}
			}
			semaforo.release();
				
			return totale;
		}
		else {
			System.out.println("Impossibile generare il report. Attendere qualche minuto e riprovare!");
			return -1;
		}
	}
}
