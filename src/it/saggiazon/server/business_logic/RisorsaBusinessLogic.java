package it.saggiazon.server.business_logic;

import java.util.ArrayList;
import java.util.Date;
import it.saggiazon.external.ExternalSystem;
import it.saggiazon.server.entity.EntityAutore;
import it.saggiazon.server.entity.EntityCatalogo;
import it.saggiazon.server.entity.EntityCatalogoAutori;
import it.saggiazon.server.entity.EntityCatalogoPreferenze;
import it.saggiazon.server.entity.EntityUtente;

public class RisorsaBusinessLogic {
	
	public ArrayList<String> listaAutoriCompleta(){
		EntityCatalogoAutori catalogoAutori = EntityCatalogoAutori.getInstance();
		return catalogoAutori.getListaAutori();				
	}
	
	public int inserisciRisorsa (int id_tipo, String autore, String titolo, Date data_pubblicazione, Float prezzo_acquisto,
			Float prezzo_noleggio, String filename) {
		
		EntityCatalogo catalogo = EntityCatalogo.getInstance();
		EntityCatalogoAutori catalogoAutori = EntityCatalogoAutori.getInstance();
		
		int result = 0;
		
		if(catalogo.checkDuplicato(titolo, autore))
			return -2;		
		
		System.out.println("[RisorsaBL]: Next Id per la nuova risorsa: "+catalogo.getNextId());
		
		result = catalogo.aggiungiRisorsa(catalogo.getNextId(), id_tipo, catalogoAutori.getAutore(autore), titolo, data_pubblicazione, 
				prezzo_acquisto, prezzo_noleggio, filename);
		
		if(result == 1) {	
			System.out.println("[RisorsaBL]: Risorsa aggiunta correttamente");
			notifica(id_tipo, catalogoAutori.getAutore(autore));			
		}
		
		return result;
	}
	
	public void notifica(int id_tipo, EntityAutore autore) {
		ArrayList<EntityUtente> listaNotifica = new ArrayList<EntityUtente>();
		listaNotifica = EntityCatalogoPreferenze.getInstance().utentiDaNotificare(id_tipo, autore);	
		ArrayList<String> listaEmail = new ArrayList<String>();
		System.out.println("<<<<"+listaNotifica.size()+">>>>");
		
		if(listaNotifica.size()>0) {
			for(EntityUtente utente:listaNotifica) {
				listaEmail.add(utente.getEmail());
			}
			
			ExternalSystem.notificaPreferenze(listaEmail, id_tipo, autore.getNomeCompleto());
		}
	}

}